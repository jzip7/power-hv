This code works by reading any .txt file in the `sensor_data` folder and moving that .txt file to the `archive` subfolder when complete. 

The files in the `processed` subfolder will be the files that need to be uploaded to the server.
 
# __Installation__
## __Install Python__

If the raspberry pi’s have Python version 3.4 +, you will be able to easily install the items in the `requirements.txt` file. This code uses Python 3.7 and it may be best to upgrade if the pi’s are not on this version. Here’s a guide to check your Python version and how to upgrade if you would like to: https://dev.to/serhatteker/how-to-upgrade-to-python-3-7-on-ubuntu-18-04-18-10-5hab
 
## __Create a Virtual Environment__

After that, we suggest creating a virtual python environment to avoid conflicts of library versions if more Python code is added in the future. 

To do that, navigate inside the predict directory and run the following terminal commands:
1. __To create the environment called predictEnv:__
```
python3 -m venv predictEnv
```
 
2. __To activate the environment:__
```
source predictEnv/bin/activate
```

There should now be a (predictEnv) prefix at the beginning of the prompt which tells us the environment is activated. To use it in the future, make sure you run the second line again to activate it.
 
** _If you ever need to deactivate the environment for any reason, just type:
            deactivate_

## __Install Required Packages__
1. __To install the packages from requirements.txt file__
```
pip install -r requirements.txt
```
 
1. __To run the prediction code manually:__
```
python fcm_predict.py
```

# __Testing__

When the `fcm_predict.py` file is run, the `2020080512.txt` file should be processed, moved to the `archive` folder, and a `2020080512_processed.txt` file should be created in the `processed` folder. 

If an `error.log` file is being produced, it means it is not finding the files on the server. Links that could be broken are in the `fcm_predict.py` file on lines 82 and 93. 

## __After Testing__

* Once the above files have been created, they can be removed since they are not using real data. 
* Uncomment line 63 and 64 (remove #) of `fcm_predict.py` once you would like to start receiving warning emails. This will send an email to the first customer who owns the current sensor. Emails should be sent to the first match in customer_sensor table if their sensor is predicted to have a condition other than `normal`.
