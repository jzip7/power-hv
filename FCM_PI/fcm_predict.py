import os
import sys
import pandas as pd
import numpy as np
import skfuzzy as fuzz
import urllib.request
import requests
import json

from sklearn import preprocessing
from joblib import load, dump
from datetime import datetime

data_folder = 'sensor_data/'
url = 'http://localhost/powerhv/'

def predict(file):
    ## file name without extension
    file_name = file[0:-4]

    df = pd.read_csv(data_folder + file, index_col=False)
    df.columns = df.columns.str.strip()

    ## convert non numeric values to NaN
    df['Temp.'] = pd.to_numeric(df['Temp.'], errors='coerce')
    df['Rel. Hum.'] = pd.to_numeric(df['Rel. Hum.'], errors='coerce')
    df['Par. Dis.'] = pd.to_numeric(df['Par. Dis.'], errors='coerce')
    df['Vib.'] = pd.to_numeric(df['Vib.'], errors='coerce')
    
    ## drop NaN values
    df = df.dropna(axis=0, how='any')

    scaled = scaler.transform(df)

    data = np.vstack((scaled[:,2], scaled[:,3]))
    # print(data)

    u, u0, d, jm, p, fpc = fuzz.cluster.cmeans_predict(data, cntr, 2, error=0.005, maxiter=1000)

    predictions = []
    dataset_size = len(u[0])

    ## save sensor id
    sensor_file = open('ID', 'r')
    sensor_id = sensor_file.readline()

    sent = False
    for i in range(dataset_size): 
        for ind, val in enumerate(u[:,i]): ## index will be 0-3
            largest = max(u[:,i])
            if val == largest:
                largest_index = ind
        if largest_index != 0 and sent == False:
            sent = True
            data = {
                'id': sensor_id,
                'condition': largest_index,
                'temp': df['Temp.'][i],
                'hum': df['Rel. Hum.'][i],
                'dis': df['Par. Dis.'][i],
                'vib': df['Vib.'][i]
            }
            # if(df['Par. Dis.'][i] > 5 or largest_index != 0):
                # req = requests.get(url + '/python/send_warning.php', params=data)

        predictions.append(largest_index)

    df['condition'] = predictions

    ## create file_name_processed.txt file with prediction column appended
    with open(data_folder + 'processed/' + file_name + '_processed.txt', 'w') as f:
        f.write(df.to_csv(header=True, index=False, sep=','))
    # ## move unprocessed file to archive folder
    os.rename(data_folder + file, data_folder + 'archive/' + file)



local_model = json.load(open('model.json'))

try:
    ## get information from model.json file on server
    model_fetch = urllib.request.urlopen(url + 'python/model.json').read()
    cloud_model = json.loads(model_fetch.decode('utf-8'))
    if local_model['id'] != cloud_model['id']:
        cntr = np.array([[cloud_model['x1'], cloud_model['y1']], 
                    [cloud_model['x2'], cloud_model['y2']], 
                    [cloud_model['x3'], cloud_model['y3']],
                    [cloud_model['x4'], cloud_model['y4']]])
        dump(cntr, 'cntr')

        ## get scaler from file on server using path from model.json file
        ## scaler value url like python/<timestamp>
        scaler_fetch = urllib.request.urlopen(url + cloud_model['scaler']).read()
        ## write it to local files
        scaler = open('scaler', 'wb')
        scaler.write(scaler_fetch)
        scaler.close()

        ## save model.json to local files
        with open('model.json', 'w') as model_file:
            json.dump(cloud_model, model_file)
except urllib.error.HTTPError:
    with open('error.log', 'a') as error_file:
        error_file.write('Failed loading new model from server ' + str(datetime.now()) + "\n")
        error_file.close()
finally:    
    ## run to make predictions, uses local files, no fetching from server
    cntr = load('cntr')
    scaler = load('scaler')

    dt = datetime.now()
    current_file = dt.strftime("%Y") + dt.strftime("%m") + dt.strftime("%d") + dt.strftime("%H")

    files = os.listdir(data_folder)

    for file in files:
        if file[-4:] == '.txt':
            predict(file)