import os
import pandas as pd
import numpy as np
import skfuzzy as fuzz

from sklearn import preprocessing
from joblib import dump
from scipy.spatial import distance
from itertools import combinations

def create_initial_u(df):
    """
        Iterate through data frame and compare each element to failure criteria.
        Classes: 0 = normal, 1 = mechanical failure, 2 = electrical failure, 3 = both.

        Returns: numpy array of each element's u score.
    """
    new_list = []
    for _, row in df.iterrows():
        if(row['vib'] == 0): ## no vibration = normal or electrical failure
            if row['dis'] < 5: ## normal
                new_list.append([1, 0, 0, 0])
            elif row['dis'] < 10: ## warning
                new_list.append([.5, 0, .5, 0])
            else: ## electrical
                new_list.append([0, 0, 1, 0])
        else: ## vibration = mechanical or both
            if row['dis'] < 5: ## mechanical
                new_list.append([0, 1, 0, 0])
            elif row['dis'] < 10: ## warning
                new_list.append([.01, .33, .33, .33])
            else: ## both
                new_list.append([0, 0, 0, 1])
    return np.array(new_list)

def define_class_num(val):
    """
        Assign class number to condition column to be used for checking accuracy.

        Returns: 0, 1, 2, 3, or -1 if the condition is invalid.
    """
    if val == 'good':
        return 0
    elif val == 'mechanical failure':
        return 1
    elif val == 'internal electrical insulation failure':
        return 2
    elif val == 'both electrical and mechanical failure':
        return 3
    else:
        return -1

def clean_conditions(dataset):
    """
        Clean the condition column of a dataset to only contain
        valid conditions.

        Returns: The cleaned dataset.
    """
    conditions = [
        'good',
        'internal electrical insulation failure',
        'mechanical failure',
        'both electrical and mechanical failure',
    ]
    dataset['condition'] = dataset['condition'].str.lower()
    dataset['condition'] = [val if val in conditions else None for ind, val in enumerate(dataset['condition'])]
    return dataset

def print_accuracy(u, labelled):
    """
        Analyze predicted u values to their actual labelled value.
    """
    incorrect_count = 0
    dataset_size = len(u[0])
    for i in range(dataset_size): 
        for ind, val in enumerate(u[:,i]): ## index will be 0-3
            largest = max(u[:,i])
            if val == largest:
                largest_index = ind
        if largest_index != labelled.iloc[i]['condition']:
            print('NO MATCH i = {}, condition = {:.0f}, largest_index = {}'.format(i, labelled.iloc[i]['condition'], largest_index))
            incorrect_count += 1
    print('accuracy on labelled data: {:.4%}'.format(1 - (incorrect_count/np.size(u, 1))))

def show_plots(data, u, cntr, title):
    """
        Show the plots of data and their centers along Discharge and Vibration axes.
    """
    import matplotlib.pyplot as plt
    import seaborn as sns

    ## Configure some general styling
    sns.set_style("white")
    plt.rcParams['font.size'] = 12
    plt.rcParams['axes.labelsize'] = 20
    # plt.rcParams['axes.labelweight'] = 'bold'
    plt.rcParams['axes.titlesize'] = 20
    plt.rcParams['xtick.labelsize'] = 15
    plt.rcParams['ytick.labelsize'] = 15
    plt.rcParams['legend.fontsize'] = 15
    plt.rcParams['figure.titlesize'] = 20
    plt.rcParams['figure.figsize'] = (8,7)

    _, ax = plt.subplots()
    ax.set_title(title)
    ax.set(xlabel='vibration', ylabel='partial discharge')

    # Plot the clusters according to their centers
    for i in range(4):
       ax.plot(data[1, u.argmax(axis=0) == i],
                data[0, u.argmax(axis=0) == i], 'o',
                label='class ' + str(i))
    # Plot the centers
    for pt in cntr:
       ax.plot(pt[1], pt[0], 'ks')

    plt.show()

def training_loop():
    """
        Import data, scale it, and use it to train the FCM algorithm.
    """
    ## Input sample data
    df_train = pd.read_csv('train_data.csv')

    ## Remove invalid conditions from data
    # dataset = clean_conditions(df_test)
    # ## Save a copy of all data
    # df_train = df_train.drop(columns=['condition'])
    # labelled = dataset.dropna(axis=0, how='any').reset_index()

    ## Create scaler object to keep scaling consistent
    scaler = preprocessing.MinMaxScaler()
    ## Scale to fit training dataset
    scaled = scaler.fit_transform(df_train)

    ## Retrieve proper columns from array
    dis = scaled[:,2]
    vib = scaled[:,3]
    ## Stack columns into one array
    disvibaud = np.vstack((dis, vib))

    ## Create list of defined u-matrix
    new_u = create_initial_u(df_train).T

    ## Determine centers of the clusters using new_u
    cntr, u, u0, d, jm, p, fpc = fuzz.cluster.cmeans(
        disvibaud, 4, 2, error=0.005, maxiter=1000, init=new_u)
    
    return cntr, u, scaler

    ## Check centers are consistent using a plot
    # show_plots(disvibaud, u, cntr, 'Trained Centers')

def accuracy_loop():
    """
        Print the accuracy of the algorithm using the sample labelled data.
    """
    ## Find accuracy using labelled data
    df_test = pd.read_csv('test.csv')
    ## Select only data with valid conditions
    labelled = clean_conditions(df_test)
    # ## Replace conditions with their coresponding number
    labelled['condition'] = [define_class_num(val) for val in labelled['condition']]

    ## Scale new data
    scaled = scaler.transform(df_test[['tem', 'hum', 'dis', 'vib', 'aud']])

    ## Retrieve proper columns from array
    dis = scaled[:,2]
    vib = scaled[:,3]
    aud = scaled[:,4]
    ## Stack columns into one array
    disvibaud = np.vstack((dis, vib, aud))

    ## Predict u-matrix
    u, u0, d, jm, p, fpc = fuzz.cluster.cmeans_predict(disvibaud, cntr, 2, error=0.005, maxiter=1000)

    ## Print accuracy of prediction
    print_accuracy(u, labelled)   

    ## Check predictions are normal using a plot
    # show_plots(disvibaud, u, cntr, 'Predicted Points')

cntr, u, scaler = training_loop()
# a = [list(cntr[0]), list(cntr[1]), list(cntr[2]), list(cntr[3])]
# print(cntr)
a = [[0.02, 0.00041, 0.41],
     [0.03, 0.98, 0.87], 
     [0.83, 0.00008, 0.85], 
     [0.83, 0.00008, 0.85]]
## check that cluster centers are seperated well, 
## keep training until they are found to be a good distance
good = False
reset = False
bad_counter = 0
while good == False:
    for combo in combinations([0, 1, 2, 3], 2):
        d = distance.euclidean(a[combo[0]], a[combo[1]])
        if d < .50: 
            bad_counter += 1
            print('bad distance')
        if bad_counter > 0:
            print('resetting centers')
            reset = True
            good = False
        else:
            print('good distance')
            good = True
    if reset == True:
        cntr, u, scaler = training_loop()
        a = [list(cntr[0]), list(cntr[1]), list(cntr[2]), list(cntr[3])]
        bad_counter = 0
        reset = False

dump(cntr, 'cntr')
dump(scaler, 'scaler')