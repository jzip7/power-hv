import os
import pandas as pd
import numpy as np
import skfuzzy as fuzz
import urllib.request

from sklearn import preprocessing
from joblib import load

cntr = load('cntr')
scaler = load('scaler')

df = pd.read_csv('data.csv')
scaled = scaler.transform(df)

data = np.vstack((scaled[:,2], scaled[:,3], scaled[:,4]))

u, u0, d, jm, p, fpc = fuzz.cluster.cmeans_predict(data, cntr, 2, error=0.005, maxiter=1000)

print(u)

conditions = [
    'Normal',
    'Mechanical Failure',
    'Electrical Insulation Failure',
    'Both Mechanical and Electrical Failure'
]
warning = ''
for j in range(len(u[0])):
    print('----- row {} -----'.format(j))
    for i in range(4):
        print('{} = {:.2%}'.format(conditions[i], u[i][j]))
        if u[i][j] > .40:
            print('warning for ' + conditions[i])
            warning = conditions[i]

print(warning)

# if u[u.argmax(axis=0)] < .50:
#     print('warning for ')

# dataset_size = len(u[0])
# for i in range(dataset_size): 
#     for ind, val in enumerate(u[:,i]): ## index will be 0-3
#         largest = max(u[:,i])
#         if val == largest:
#             largest_index = ind