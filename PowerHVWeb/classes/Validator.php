<?php

namespace classes;

class Validator
{
    private $errors = [];
  
  /**
   * Required fields check
   * @param  String $field the filed name
   * @return void
   */
    public function required($field)
    {
        if($field == 'available'){
            if(filter_input(INPUT_POST, $field) == ''){
                $this->setError($field, 'Available is a required field');
            }
        }elseif (!filter_input(INPUT_POST, $field)) {
            if($field == 'phone1'){
                $this->setError($field, 'Phone is a required field');
            }else{
                $this->setError($field, label($field) . ' is a required field');
            }            
        }
    }
    
    public function fileRequired($field)
    {
        if ($_FILES[$field]['name'] == '') {
            $this->setError($field, label($field) . ' is a required field');
        }
    }
  
  /**
   * Only alpalphabet allowed fields check
   * @param  String $field the filed name
   * @return void
   */
    public function alphabet($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            $pattern = '/[^a-zA-Z\s]/';
            if (preg_match($pattern, filter_input(INPUT_POST, $field))) {
                $this->setError($field, label($field) . ' contains special character(s) or numbers');
            }
        }
    }
  
  /**
   * No special chars allowed fields check
   * @param  String $field the filed name
   * @return void
   */
    public function specialChars($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            $pattern = '/[^a-zA-Z0-9\s]/';
            if (preg_match($pattern, filter_input(INPUT_POST, $field))) {
                $this->setError($field, label($field) . ' contains special character(s)');
            }
        }
    }
  
  /**
   * Email field check
   * @param  String $field the filed name
   * @return void
   */
    public function email($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            if (!filter_var(filter_input(INPUT_POST, $field), FILTER_VALIDATE_EMAIL)) {
                $this->setError($field, label($field) . ' should be in this format: name@example.com');
            }
        }
    }
  
  /**
   * Postal code field check
   * @param  String $field the filed name
   * @return void
   */
    public function postalCode($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            if (!preg_match("/^[A-Z][0-9][A-Z]\s[0-9][A-Z][0-9]$/", filter_input(INPUT_POST, $field))) {
                $this->setError($field, label($field) . ' must be in this format: A1A 1A1');
            }
        }
    }
  
  /**
   * Phone field check
   * @param  String $field the filed name
   * @return void
   */
    public function phone($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            if (!preg_match("/^\([0-9]{3}\)\s?[0-9]{3}\-[0-9]{4}$/", filter_input(INPUT_POST, $field)) &&
                !preg_match("/^[0-9]{3}\.[0-9]{3}\.[0-9]{4}$/", filter_input(INPUT_POST, $field)) &&
                !preg_match("/^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$/", filter_input(INPUT_POST, $field))) {
                $str1 = 'must be in these formats: (204) 555-1212 or 204.444.2323 or 204-555-3434';
                $this->setError($field, label($field) . $str1);
            }
        }
    }
  
  /**
   * Password complixty check
   * @param  String $field the filed name
   * @return void
   */
    public function passwordComplixty($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            $pattern = '/(?=.*[[:punct:]]+)(?=.*[A-Z]+)(?=.*[0-9]+).{8,}/';
            if (!preg_match($pattern, filter_input(INPUT_POST, $field))) {
                $str2 = ' must be at least 8 characters and contains a special ';
                $str3 = 'character and a uppercase letter and a number';
                $this->setError($field, label($field) . $str2 . $str3);
            }
        }
    }
  
  /**
   * Password confirmation check
   * @param  String $field the filed name
   * @return void
   */
    public function passwordConfirmation($field1, $field2)
    {
        
        if (filter_input(INPUT_POST, $field1) &&
        filter_input(INPUT_POST, $field2) &&
        empty($this->errors[$field1])) {
            if (filter_input(INPUT_POST, $field1)!=filter_input(INPUT_POST, $field2)) {
                $this->setError($field2, 'The two passwords you typed do not match');
            }
        }
    }
  
  /**
   * Gender field check
   * @param  String $field the filed name
   * @return void
   */
    public function gender($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            if (strtolower(filter_input(INPUT_POST, $field))!='m' &&
                strtolower(filter_input(INPUT_POST, $field))!='f') {
                $this->setError($field, label($field) . ' must be \'m\' or \'f\'');
            }
        }
    }
  
  /**
   * Age field check
   * @param  String $field the filed name
   * @return void
   */
    public function age($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            if (!filter_var($_POST[$field], FILTER_VALIDATE_INT)) {
                $this->setError($field, label($field) . ' must be an integer');
            } elseif (filter_input(INPUT_POST, $field) < 18) {
                $this->setError($field, 'You must be at least 18 years old');
            }
        }
    }
    
    public function costAndPrice($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            if (!filter_var($_POST[$field], FILTER_VALIDATE_FLOAT)) {
                $this->setError($field, label($field) . ' must be a float number');
            } elseif (filter_input(INPUT_POST, $field) <= 0) {
                $this->setError($field, label($field) . ' must be a positive number');
            }
        }
    }
    
    public function functionID($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            $pattern = '/[1-5]{1}/';
            if (!preg_match($pattern, filter_input(INPUT_POST, $field))) {
                $this->setError($field, label($field) . ' can only be a single interger between 1 and 5');
            }
        }
    }
    
    public function remainingStock($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            if (!filter_var($_POST[$field], FILTER_VALIDATE_INT)) {
                $this->setError($field, label($field) . ' must be an integer');
            } elseif (filter_input(INPUT_POST, $field) < 0) {
                $this->setError($field, label($field) . 'must be a positive number');
            }
        }
    }
    
    public function availableBoolean($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            $pattern = '/[0-1]{1}/';
            if (!preg_match($pattern, filter_input(INPUT_POST, $field))) {
                $this->setError($field, label($field) . ' can only be 0 or 1');
            }
        }
    }
    
    public function creditNum($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            $pattern = '/^[0-9]{16}$/';
            if (!preg_match($pattern, filter_input(INPUT_POST, $field))) {
                $this->setError($field, label($field) . ' can only contain 16 numbers');
            }
        }
    }
    
    public function securityNum($field)
    {
        if (filter_input(INPUT_POST, $field)) {
            $pattern = '/^[0-9]{3}$/';
            if (!preg_match($pattern, filter_input(INPUT_POST, $field))) {
                $this->setError($field, label($field) . ' can only contain 3 numbers');
            }
        }
    }
  
  /**
   * Return the errors array
   * @return Array
   */
    public function errors()
    {
        return $this->errors;
    }
  
  /**
   * Set error message if a field doesn't meet requirement
   * @param String $field the field to set the error for
   * @param String $message the message
   * return void
   */
    private function setError($field, $message)
    {
        $this->errors[$field] = $message;
    }
}
