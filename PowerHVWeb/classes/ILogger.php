<?php


namespace classes;

/**
 * Interface ILogger
 * methods to implement for Database of File Logger
 */
interface ILogger
{

    public function write($event);
    public function read($limit);

// end class
}
