<?php
namespace CMail;
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer.php';
require 'SMTP.php';
require 'Exception.php';

class Mail{
    
    private $mail;

    function __construct($host, $userName, $password, $portNumber, $from, $fromName, $replyTo, $replyName){
        // // Instantiation and passing `true` enables exceptions   
        $this->mail = new PHPMailer(true);

        $test_host = (object) Array(
            'Host'     => 'hwsmtp.exmail.qq.com',
            'Username' => 'test@vipdaigou.cn',
            'Password' => 'FkrqqfWU2mrcmCBf',
            'Port'     => 465,
            'From'     => 'test@vipdaigou.cn',
            'FromName' => 'PowerHV Webmaster',
            'ReplyTo'  => 'test@vipdaigou.cn',
            'ReplyName'=> 'PowerHV Webmaster',
        );

        //Server settings
        // $this->mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $this->mail->isSMTP();                                            // Send using SMTP
        $this->mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        
        if(is_null($host) || empty($host) ||
        is_null($userName) || empty($userName) ||
        is_null($password) || empty($password) ||
        is_null($portNumber) || empty($portNumber) ||
        is_null($from) || empty($from) ||
        is_null($fromName) || empty($fromName) ||
        is_null($replyTo) || empty($replyTo) || 
        is_null($replyName) || empty($replyName)
        ){
            $this->mail->Host         = $test_host->Host;                    // Set the SMTP server to send through
            $this->mail->Username     = $test_host->Username;                // SMTP username
            $this->mail->Password     = $test_host->Password;                // SMTP password
            $this->mail->Port         = $test_host->Port;                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
            $this->mail->setFrom($test_host->From, $test_host->FromName);
            $this->mail->addReplyTo($test_host->ReplyTo, $test_host->ReplyName);
            $this->mail->addBCC($test_host->From);
        
        } else {
            $this->mail->Host         = $host;                    // Set the SMTP server to send through
            $this->mail->Username     = $userName;                // SMTP username
            $this->mail->Password     = $password;                // SMTP password
            $this->mail->Port         = $portNumber;              // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
            //Recipients
            $this->mail->setFrom($from, $fromName);
            $this->mail->addReplyTo($replyTo, $replyName);
            $this->mail->addBCC($from);
        }
        // echo "Construction finished.";
        // var_dump($this->mail);
    }

    function sendMail($message){
        $timeStamp = date('c');
        $test_message = (object) array(
            'Address' => 'target@ncai.ca',
            'Name'    => 'Customer',
            'Subject' => "Password Reset Email From https://PoweHV.com",
            'Body'    => "<p>HTML email body with <b> Tags </b> </p><p> Sent at {$timeStamp}</p>",
            'AltBody' => "Plain text message",
        );
        
        if(is_null($message) || empty($message))
            $message = $test_message;

        try {
            // Content
            $this->mail->isHTML(true); 
            $this->mail->addAddress($message->Address, $message->Name);                                 // Set email format to HTML
            $this->mail->Subject = $message->Subject;
            $this->mail->Body    = $message->Body; //'This is the HTML message body <b>in bold!</b>';
            $this->mail->AltBody = $message->AltBody;'This is the body in plain text for non-HTML mail clients';

            $this->mail->send();
            $responseMsg = '<p>Password reset email has been sent to your email, ';
            $responseMsg = $responseMsg . 'if you has an account with this email in our system.</p>';
            $responseMsg = $responseMsg . '<p> Please click the link provide in the email to reset your password.</p>';
            echo $responseMsg;
        } catch (Exception $e) {
            echo "<p>Message could not be sent.</p>";
            echo "<p>This Error has been logged and our specialist will investigate it asap.</p>";
            echo "<p>Please try again later or <a href='http://www.power-hv.com/contact.php' >contact us </a>.";
            error_log("Mailer Error: {$this->mail->ErrorInfo}",0);
        }
    }
}
// use CMail\Mail;
// function pwd_resetMail($address, $name, $html, $text){    
//     require 'CMail.php';
//    $email = new Mail(null, null, null, null, null, null, null, null);
    
//     $timeStamp = date('c');
//     $message = (object) array(
//         'Address' => $address,
//         'Name'    => $name,
//         'Subject' => "Password Reset Email From https://PoweHV.com",
//         'Body'    => "<p>Hi {$name},</p>" . $html ."<p> Sent at {$timeStamp}</p>",
//         'AltBody' => $text,
//     );
    
//     $email->sendMail($message);
// }
