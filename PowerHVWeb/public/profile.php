<?php


include __DIR__ . '/../config/config.php';


// Any page variables
$page = 'Profile';
// Any page includes

// If user doesn't log in
if (empty($_SESSION['logged_in'])) {
    setFlash('error', 'You must be logged into to see the profile page');
    header('Location: Log_in.php');
    die;
}

// If doesn't get user_id in URL
if (empty(cleanGET('user_id')) && empty($_SESSION['user_id'])) {
    die('There are some problems when getting your information from database');
}

// If user_id in URL is empty
if (empty(cleanGET('user_id'))) {
    $id = intval($_SESSION['user_id']);
} else { // If user_id in $_SESSION is empty
    $id = intval(cleanGET('user_id'));
}

$query = "SELECT * FROM customer 
          WHERE customer_id = :customer_id";

// prepare the query
$stmt = $dbh->prepare($query);

// Prepare params array
$params = array(
    ':customer_id' => $id
);

// execute the query
$stmt->execute($params);

// get the result
$result = $stmt->fetch(PDO::FETCH_ASSOC);

?><!doctype html>
<html lang="en">

<?php require_once __DIR__ . '/../inc/index_head.php'; ?>
<link rel="stylesheet" href="/css/profile.css">
<link rel="stylesheet" href="/css/show.css">
<style>
    h1.registration_information_title{
        margin-left: 280px;
        font-size: 20px;
    }
    
    table.registration_information{
        margin-left: 280px;
    }
    
    table.registration_information th{
        font-size: 20px;
    }
    td.value{
        font-size: 20px;
    }
    
    h2.back_to_home_page_h2{
        margin-left: 280px;
    }
</style>
    
<main>
    <?php require_once __DIR__ . '/../inc/header.php'; ?>
    <?php include __DIR__ . '/../inc/flash.inc.php'; ?>
    <h1 class="registration_information_title">Registration Information:</h1>

    <?php if ($result) : ?> 
    <!-- Foreach loop to output result -->
    <table class="registration_information">
        <tr>
            <th>Field</th>
            <th>Value</th>
        </tr>
        <?php foreach ($result as $key => $value) : ?>
        <!-- avoid outputing customer_id and password -->
            <?php if ($key!='customer_id' && $key!='pw' && $key!= 'is_admin') :?>
            <tr>            
                <td><strong><?=label($key)?></strong></td>
                <td class="value"><?=$value?></td>            
            </tr>
            <?php endif; ?>
        <?php endforeach; ?>    

    </table>
    <h2 class="back_to_home_page_h2"><a class="back_to" href="index.php">Back to home page</a></h2>
  
    <?php else : ?>
    <h2>Sorry there was a problem for registration</h2>

    <?php endif; ?>
</main>
<!-- Include footer.php -->
<?php require_once __DIR__ . '/../inc/modal.php'; ?>
<?php require_once __DIR__ . '/../inc/footer.php'; ?>
</html>