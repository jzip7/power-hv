<?php

$dbh = new PDO('mysql:host=localhost;dbname=powerhv', 'web_user', 'mypass');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$query = 'SELECT * FROM sensor_data ORDER BY created_at DESC LIMIT 1;';
$params = array();
$stmt = $dbh->prepare($query);
$stmt->execute($params);
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

header('Content-Type: application/json');
echo json_encode($result);