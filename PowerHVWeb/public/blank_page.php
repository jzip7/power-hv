<?php

include __DIR__ . '/../config/config.php';

// Any page variables
$page = 'Index';

// sign_out if
if (filter_input(INPUT_GET, 'logout')){
    unset($_SESSION['user']);
    unset($_SESSION['role']);
    unset($_SESSION['csrf_token']);
    unset($_SESSION['user_name']);
    unset($_SESSION['cart']);
    session_regenerate_id();
    setFlash('success', 'You have successfully logged out!');
    header('location:index.php');
    die;
}
     

?><!doctype html>
<html lang="en">

<?php require_once __DIR__ . '/../inc/index_head.php'; ?>
<!--link rel="stylesheet" href="/css/products.css"-->
  
<body>
    
    <div id="wrapper">
        <?php require_once __DIR__ . '/../inc/header.php'; ?>
        <?php include __DIR__ . '/../inc/flash.inc.php'; ?>
        
              
        <div class="break"></div>
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>