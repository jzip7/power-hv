<?php

// Assign a variable name for each of pages
$page = 'Update';
$style = 'pa';
$page_name = 'Update | Power HV';

// Include header and navigation  inc.php
include __DIR__ . '/../config/config.php';

// If REQUEST METHOD is POST
if ('GET' == $_SERVER['REQUEST_METHOD']) {
   
    // If no error, insert record into database

        try {
            // Create query
            $query = "INSERT INTO
                      sensor_data
                      (sensor_id, Temp, Hum, Dis, Vib, created_at, updated_at)
                      VALUES
                      (:sensor_id, :temp, :hum, :dis, :vib, now(), now())";

            // Prepare query
            $stmt = $dbh->prepare($query);
      
            // Bind placeholders with inserted values
            $params = array(
                ':sensor_id' => $_GET['ID'],
                ':temp' => $_GET['temp'],
                ':hum' => $_GET['humid'],
                ':dis' => $_GET['current'],
                ':vib' => $_GET['VBatt']
            );

            // execute query
            $stmt->execute($params);
        

            // Redirect
            //header('Location: profile.php?user_id=' . $customer_id);
            //exit;
        } catch (Exception $e) {
            die($e->getMessage());
        }
}// end IF POST