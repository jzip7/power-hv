<?php

include __DIR__ . '/../config/config.php';


// Any page variables
 $page = 'Products';
// Any page includes

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../inc/index_head.php'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="/css/products.css">
<style>
    h2#product_title{
        font-size: 5rem;
        font-family: 'Barlow Condensed', sans-serif;
    }
</style>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../inc/header.php'; ?>
        <?php include __DIR__ . '/../inc/flash.inc.php'; ?>
        <div class="main-banner2">
            <div class="container">
                <div class="twelve column">
                    <div class="banner">
                        
                            <h2 id="product_title">
                                Products 
                            </h2> 
                       
                    </div>
                </div>
            </div>
        </div>    
        
        <br />
        <br />
        <br />
        <br />
        <br />
        <div class="container">
            <div>
                <h2>Transformer Bushings:</h2>
                <div class="row">                    
                    <div class="col-6 product_block">
                        <div>
                            <img class="product_style" src="img/energy_efficient_bushings.png" alt="transformer_bushings_energy_efficient_bushings.png" />
                        </div>
                        <span class="text">Energy Efficient Bushings</span><br/>
                        <a href="/show.php?product_id=1">More</a>   
                    </div>  
                    <div class="col-6 product_block">
                        <div>
                            <img class="product_style" src="img/energy_efficient_bushings.png" alt="transformer_bushings_regular_bushings.png" />
                        </div>
                        <span class="text">Regular Bushings</span><br/>
                        <a href="/show.php?product_id=2">More</a>   
                    </div>
                </div>            
            </div>
            
            <br />
            <br />
            <br />
            <br />
            <br />
            <div>
                <h2>Type-C Bushings:</h2>
                <div class="row">                
                    <div class="col-6 product_block">
                        <div>
                            <img class="product_style" src="img/energy_efficient_bushings_Type_C.png" alt="type-c_bushings_energy_efficient_bushings.png" />
                        </div>
                        <span class="text">Energy Efficient Bushings</span><br/>
                        <a href="/show.php?product_id=3">More</a>   
                    </div>
                    
                    <div class="col-6 product_block">
                        <div>
                            <img class="product_style" src="img/energy_efficient_bushings_Type_C.png" alt="type-c_bushings_regular_bushings.png" />
                        </div>
                        <span class="text">Regular Bushings</span><br/>
                        <a href="/show.php?product_id=4">More</a>   
                    </div>
                    
                </div>            
            </div>
            
            <br />
            <br />
            <br />
            <br />
            <br />            
            <div>
                <h2>Hardware:</h2>
                <div class="row">   
                    
                    <div class="col-6 product_block">
                        <div>
                            <img class="product_style product_width" src="img/connectors.jpg" alt="hardware_connectors.jpg" />
                        </div>
                        <span class="text">Connectors</span><br/>
                        <a href="/show.php?product_id=5">More</a>   
                    </div>
                    
                    <div class="col-6 product_block">
                        <div>
                            <img class="product_style product_width" src="img/corona_rings.png" alt="hardware_corona_rings.jpg" />
                        </div>
                        <span class="text">Corona Rings</span><br/>
                        <a href="/show.php?product_id=6">More</a>   
                    </div>
                </div>            
            </div>
            
            <br />
            <br />
            <br />
            <br />
            <br />
            <div>
                <h2>Sensors:</h2>
                <div class="row">                
                    <div class="col-6 product_block">
                        <div>
                            <img class="product_style product_width" src="img/sensor_2.png" alt="sensor.png" />
                        </div>
                        <span class="text">Sensors</span><br/>
                        <a href="/show.php?product_id=7">More</a>   
                    </div>               
                </div>            
            </div>
            
        </div>
        
       
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>