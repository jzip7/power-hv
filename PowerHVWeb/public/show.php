<?php

include_once __DIR__ . '/../config/config.php';
include_once __DIR__ . '/../lib/functions.php'; 

// Any page variables
$page = 'Show';

// If product_id in URL is not empty
if (!empty($_GET['product_id'])) {
    $id = intval($_GET['product_id']);
}

$query = "SELECT *
          FROM 
          products 
          WHERE 
          product_id = :product_id";

// prepare the query
$stmt = $dbh->prepare($query);

// Prepare params array
$params = array(
    ':product_id' => $id
);

// execute the query
$stmt->execute($params);

// get the result
$result = $stmt->fetch(PDO::FETCH_ASSOC);

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../inc/index_head.php'; ?>
<?php header('Content-Type: text/html; charset=utf-8'); ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="/css/show.css">
<style>
    .show_product_style{
        width: 70%;
        height: 70%;
    }
</style>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../inc/header.php'; ?>
        <?php include __DIR__ . '/../inc/flash.inc.php'; ?>
        <div class="main-banner2">
            <div class="container">
                <div class="twelve column">
                    <div class="banner">
                        
                            <h2>
                                <?=label($result['name'])?> 
                            </h2> 
                       
                    </div>
                </div>
            </div>
        </div>    
    
        <div class="container">
            <div>
                <div class="row">
                    <?php if ($result) : ?> 
                    <div class="col-6 product_block">
                        <div>
                            <img class="product_style show_product_style" src="img/<?=$result['img_name']?>" alt="<?=$result['name']?>" <?=($result['img_name'] == 'sensor.jpeg') ? 'width="440px" height="300px"' : ''?>/>
                        </div>                       
                    </div>  
                    <div class="col-6 product_block output_description">
                        <div>
                            <?php foreach ($result as $key => $value) : ?>
                                <?php if ($key!='product_id' && $value != '' && $key != 'img_name'):?>
                                <ul>
                                    <li><strong><?=label($key)?>:</strong> <?=$value?></li>
                                </ul>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            
                        </div>
                        
                        <div id="add_to_cart" class="some_buttons">Interested? Click email icon to contact us</div>
                        <a href="contact.php" id="email_1" title="Email Power HV"><img src="/img/email.png" class="email_1" width="150px" height="100px" /></a>
                        
                    </div>
                    
  
                    <?php else : ?>
                    <h2>Sorry there was a problem for Product Information</h2>
                    
                    <?php endif; ?>
                    
                    <?php foreach ($result as $key => $value) : ?>
                        <?php if($value == 'Energy Efficient Bushings') :?>
                        <img src="img/parameters.png"/>
                        <?php elseif($value == 'Regular Bushings') :?>
                        <img src="img/parameters2.png"/>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    
                    <h2><a class="back_to" href="products.php">Back to Products page</a></h2>
                </div>            
            </div>
           
        </div>
        
       
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>