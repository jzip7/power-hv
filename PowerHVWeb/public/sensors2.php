<?php
$page = 'sensors'; 
?>
<!DOCTYPE HTML>
<html>
<head>
<script>
    window.onload = function() {
        var Data;
        function loadAllData() {
            var xhr = new XMLHttpRequest;
            xhr.open('GET', 'data.php');
            xhr.onreadystatechange = function() {
                if(xhr.readyState == 4 && xhr.status == 200) {
                    Data = JSON.parse(xhr.responseText);
                    //outputData(Data);
                    document.getElementById('sensor_id').innerHTML = Data[0]['sensor_id'];
                }
            }
            xhr.send(null);
            
            var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
            var today  = new Date();

            document.getElementById('date').innerHTML = today.toLocaleString(); 
        }
        
        setInterval(loadAllData,1000);

        function outputData(Data) {
            
            var table = document.getElementById('data');
            Data.forEach(function(eachline){
                
                var tr = document.createElement('tr');
                var td1 = document.createElement('td');
                var td2 = document.createElement('td');
                var td3 = document.createElement('td');
                var td4 = document.createElement('td');
                var sensor_id = document.getElementById('sensor_id');
              
                td1.innerHTML = eachline.Temp + "&nbsp&nbsp&nbsp";             
                tr.appendChild(td1);
                td2.innerHTML = eachline.Hum + "&nbsp&nbsp&nbsp";             
                tr.appendChild(td2);
                td3.innerHTML = eachline.Dis + "&nbsp&nbsp&nbsp";             
                tr.appendChild(td3);
                td4.innerHTML = eachline.Vib;             
                tr.appendChild(td4);
                table.appendChild(tr);  
                
                sensor_id.innerHTML = eachline.sensor_id;
            });
        }

        function TemperatureDraw(){
            
            var dataPoints = [];

            var xValue = 0;
            var yValue = 0;
            
            function updateChart() {//-0.5 到 0.5
                yValue = parseFloat(Data[0]['Temp']);
                dataPoints.push({ x: xValue, y: yValue });
                xValue++;
                chart.render();
            };


            var chart = new CanvasJS.Chart("TemperatureContainer", {
                theme: "light2",
                title: {
                    text: "Temperature"
                },
                axisX:{
                    title: "Time in second"
                },
                axisY:{
                    includeZero: false,
                    suffix: "℃"
                },
                data: [{
                    type: "line",
                    yValueFormatString: "#,##0.0#",
                    toolTipContent: "{y} ℃",
                    dataPoints: dataPoints
                }]
            });
            chart.render();        
            setInterval(function () { updateChart() }, 1000);
        }
        
        function HumidityDraw(){
     
            var dataPoints = [];

            var xValue = 0;
            var yValue = 0;
            
            function updateChart() {//-0.5 dao 0.5
                yValue = parseFloat(Data[0]['Hum']);
                dataPoints.push({ x: xValue, y: yValue });
                xValue++;
                chart.render();
            };


            var chart = new CanvasJS.Chart("HumidityContainer", {
                theme: "light2",
                title: {
                    text: "Humidity"
                },
                axisX:{
                    title: "Time in second"
                },
                axisY:{
                    includeZero: false,
                    suffix: "%"
                },
                data: [{
                    type: "line",
                    yValueFormatString: "#,##0.0#",
                    toolTipContent: "{y} %",
                    dataPoints: dataPoints
                }]
            });
            chart.render();        
            setInterval(function () { updateChart() }, 1000);
        }
        
        function VibrationDraw(){
                   
            var dataPoints = [];

            var xValue = 0;
            var yValue = 0;
            
            function updateChart() {//-0.5 dao 0.5
                yValue = parseFloat(Data[0]['Vib']);
                dataPoints.push({ x: xValue, y: yValue });
                xValue++;
                chart.render();
            };


            var chart = new CanvasJS.Chart("VibrationContainer", {
                theme: "light2",
                title: {
                    text: "Vibration"
                },
                axisX:{
                    title: "Time in second"
                },
                axisY:{
                    includeZero: false,
                    suffix: "Hz"
                },
                data: [{
                    type: "line",
                    yValueFormatString: "#,##0.0#",
                    toolTipContent: "{y} Hz",
                    dataPoints: dataPoints
                }]
            });
            chart.render();        
            setInterval(function () { updateChart() }, 1000);
        }
        
        function PartialDischargeDraw(){
                   
            var dataPoints = [];

            var xValue = 0;
            var yValue = 0;
            
            function updateChart() {//-0.5 dao 0.5
                yValue = parseFloat(Data[0]['Dis']);
                dataPoints.push({ x: xValue, y: yValue });
                xValue++;
                chart.render();
            };


            var chart = new CanvasJS.Chart("PartialDischargeContainer", {
                theme: "light2",
                title: {
                    text: "Partial Discharge"
                },
                axisX:{
                    title: "Time in second"
                },
                axisY:{
                    includeZero: false,
                    suffix: "V"
                },
                data: [{
                    type: "line",
                    yValueFormatString: "#,##0.0#",
                    toolTipContent: "{y} V",
                    dataPoints: dataPoints
                }]
            });
            chart.render();        
            setInterval(function () { updateChart() }, 1000);
        }
        
        TemperatureDraw();
        HumidityDraw();
        VibrationDraw();
        PartialDischargeDraw();        
    }
</script>
<style>
    .sidebar{
        float: right;
        position: relative;
        left: -78%;
    }
    
    .sidebar div{
        border: 2px solid black;
        width: 250px;
        margin-top: 15px;
        margin-bottom: 15px;
        font-size: 25px;
        padding: 10px;
    }
</style>
</head>
<body>
    <?php require_once __DIR__ . '/../inc/index_head.php'; ?>
    <?php require_once __DIR__ . '/../inc/header.php'; ?>
    <?php include __DIR__ . '/../inc/flash.inc.php'; ?>
    <!--table id="data" style="font-size: 8rem; margin-left: 25%;">
        <tr>
            <td>Temp</td>
            <td>Hum</td>
            <td>Dis</td>
            <td>Vib</td>
        </tr>
    </table-->
    <br />
    <div class="sidebar">
        <div>
            Date: <span id="date"></span>
        </div>
        <div>
            Company: ABC Hydro
        </div>
        <div>
            Site: Winnipeg Substation
        </div>
        <div>
            Sensor ID: <span id="sensor_id"></span>
        </div>
        <div>
            GPS: N45.45, E32.11
        </div>
        <div>
            Display Energy Measure
        </div>
        <div>
            View: Graph
        </div>
        <div>
            View: Data
        </div>
        <div>
            View: Alarms
        </div>
        <div>
            View: Report
        </div>
        <div>
            Download
        </div>
        <div>
            <a href="javascript:window.print()">Download: Report PDF</a>
        </div>
        <div>
            Download: Data
        </div>
    </div>
    <div id="TemperatureContainer" style="height: 370px; width: 25%; margin-left: 25%; display: inline-block; float:left;"></div>
    
    <div id="HumidityContainer" style="height: 370px; width: 25%; margin-left: 50%;"></div>
    <br />    
    <div id="PartialDischargeContainer" style="height: 370px; width: 25%; margin-left: 25%; float:left;"></div>
    
    <div id="VibrationContainer" style="height: 370px; width: 25%; margin-left: 50%;"></div>
    
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</html>