<?php

include __DIR__ . '/../config/config.php';


// Any page variables
 $page = 'About';
// Any page includes

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../inc/index_head.php'; ?>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../inc/header.php'; ?>
        <?php include __DIR__ . '/../inc/flash.inc.php'; ?>
        <div class="main-banner2">
            <div class="container">
                <div class="twelve column">
                    <div class="banner">
                            <h2>
                                About us 
                            </h2> 
                    </div>
                </div>
            </div>
        </div>
        

        <div class="container" style="text-align:center;font-size:35px;margin-top:25px;">
           
            <p>Through our patented technology we help our customers increase the efficiency of the grid as well as improving its stability.</p>
             
        </div>
        <div style="justify-content:center; align-items:center; ">
            <img class="about-banner" src="img/cutting_line.png" style="width: 95%;" />
        </div>
        
        
        <div class="container">
            <div style="font-size:35px;margin-top:25px;width:60%; height:40%;float:left;display:block;" >

                <p style="display;block;" >Through our team's extensive experience in the field, the team at Power HV is creating ground-breaking technology, decreasing greenhouse gas emissions and saving money along the way.</p>
                <br>
                <br>
                <p>Based out of St. John’s, Newfoundland & Labrador we have a unique oppurtunity to test our products in challenging weather as well be part of the some of the biggest energy generating projects in North America.</p>

            </div>
            
            <div style="justify-align">
                <img src="img/IMG_3488.JPG" style="width:40%; height:40%;float:left;display:inline-block;">
            </div>
            
            

        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="container">
            <div class="twelve column">
                <div class="registration">
                    <div class="text">
                        <h3>We are a proud member of:</h3>
                    </div>
                    <div class="member">
                        <img src="img/cigre.png" alt="International Council on Large Electric Systems" title="International Council on Large Electric Systems">
                        <a title="The Canadian Federation of Independent Business"><img src="img/cfib.png" alt="The Canadian Federation of Independent Business" title="The Canadian Federation of Independent Business"></a>
                    </div>
                    <div class="member member2">
                        <div style="height: 100px;">
                            <img src="img/apegm.png" title="Association of Professional Engineers and Geoscientists Manitoba" alt="Association of Professional Engineers and Geoscientists Manitoba">
                        </div>
                        <img src="img/plug_and_play.png" title="Plug and Play Tech Center" alt="Plug and Play Tech Center">
                    </div>
                    <div class="member member3">
                        <div style="height: 90px;">
                            <img src="img/neia.png" alt="Newfoundland & Labrador Environmental Industry Association" title="Newfoundland & Labrador Environmental Industry Association">
                        </div>
                        <img src="img/meia.png" alt="Manitoba Environmental Industries Association" title="Manitoba Environmental Industries Association">
                    </div>
                    <div class="member">
                        <img src="img/clean_tech_open.png" title="The Cleantech Open" alt="The Cleantech Open">
                    </div>                       
                </div>
            </div>
        </div>

        <div class="break"></div>
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>
