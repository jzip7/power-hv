$(document).ready(function() {
   var $signin = $('.show-modal');
   var $modal = $('#login-frame');
   var $overlay = $('#overlay');

   $signin.click(function() {
      $modal.addClass('isvisible');
      $overlay.addClass('isvisible');
   });

   $overlay.on('click', function(event) {
      if ($(event.target).is($overlay) || $(event.target).is('.isvisible')) {
         $modal.removeClass('isvisible');
         $overlay.removeClass('isvisible');
      }
   });

   $(document).keyup(function(event) {
      if (event.which == '27') {
         $modal.removeClass('isvisible');
         $overlay.removeClass('isvisible');
      }
   });
});
