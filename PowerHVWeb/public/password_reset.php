<?php

include __DIR__ . '/../config/config.php';
include __DIR__ . '/../classes/CMail.php';


// sign_out if
if (filter_input(INPUT_GET, 'logout')){
    unset($_SESSION['user']);
    unset($_SESSION['role']);
    unset($_SESSION['csrf_token']);
    unset($_SESSION['user_name']);
    unset($_SESSION['cart']);
    session_regenerate_id();
    setFlash('success', 'You have successfully logged out!');
    header('location:index.php');
    die;
}
     

use classes\Validator;
use CMail\Mail;
require_once './../classes/CMail.php';
function pwd_resetMail($address, $name, $html, $text){    
   
   $email = new Mail(null, null, null, null, null, null, null, null);
    
    $timeStamp = date('c');
    $message = (object) array(
        'Address' => $address,
        'Name'    => $name,
        'Subject' => "Password Reset Email From https://Power-HV.com",
        'Body'    => "<p>Hi {$name},</p>" . $html ."<p> Sent at {$timeStamp}</p>",
        'AltBody' => $text,
    );
    
    $email->sendMail($message);
}


// Any page variables
$page = 'PasswordReset';
// Any page includes

// If user clicked log out link, call log_out function
if (filter_input(INPUT_GET, 'logout')) {
    log_out();
    header('Location: Log_in.php');
    die;
}

$v = new Validator();

if ('POST' == filter_input(INPUT_SERVER, 'REQUEST_METHOD')) {

    if(!empty(filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL))){        
        // Required fields check
        $v->required('email');
        
        if (!$v->errors()) {
            // Get stored password from database
            $query = "select * from customer where email = :email";
            $stmt = $dbh->prepare($query);
            $params = array(
            ':email' => filter_input(INPUT_POST, 'email')
            );
            $stmt->execute($params);
            $user = $stmt->fetch(PDO::FETCH_ASSOC);
            // Only if we find provided email in database
            if ($user) {
                // var_dump($user);
                $activation_key = random_int(100000,999999);
                try{
                    $query = "update customer set activation_key ={$activation_key} where email=:email";
                    $stmt = $dbh->prepare($query);
                    $params = array(
                        ':email' => $user['email']
                        );
                    $stmt->execute($params);

                    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
                        $url = "https://";   
                    else  
                        $url = "http://";   
                    $url.= $_SERVER['HTTP_HOST'];   
               
                    $url.= $_SERVER['REQUEST_URI'];    
                    $link = $url . "/password_reset.php?email=" . $user['email'] . '&&key=' . $activation_key;
                    $text = "Please paste this link to your brower: " .$link . "to reset your password.";
                    $html = "<p>Please click <a href=" . $link . "> here </a> to reset your password </p><p>" . $text . "</p>";
                    // TODO: get customer name
                    $name = 'Customer';
                    pwd_resetMail($user['email'], $user['first_name'], $html, $text);
                    setFlash('success', 'If we got your email in our system, an email with password reset link will be sent to you.');
                }catch(Exception $e){
                    setFlash('error', 'Database error');

                }
            } //  end if has user
        } // end if no errors
        header("Location: password_reset.php");
        return;
    } elseif(isset($_POST['password']) && isset($_POST['password_confirm'])){
        $password = $_POST['password'];
        $password_confirm = $_POST['password_confirm'];        
        if($password !== $password_confirm){
            setFlash('error', 'Password mismatch');
        }
        if( !isset($_SESSION['user_id']) || empty( $_SESSION['user_id'])){
            setFlash('error', 'Session timeout, please try again');
            die();
        }
        $customer_id = $_SESSION['user_id'];
        // password complixty validation
        $v->passwordComplixty('password');
        $errors = $v->errors();

        // If no error, insert record into database
        if (!$errors) {
            try {
                $query = "update customer set pw=:password where customer_id=:customer_id";
                $stmt = $dbh->prepare($query);
                $params = array(
                    ':customer_id' => $customer_id,
                    ':password' => $password,
                    );
                $stmt->execute($params);

                setFlash('success', 'Your password has been reset');
                header('Location: Log_in.php');
                die();
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }else{
        setFlash('error', 'Something went wrong, please try again.');
        header("Location: password_reset.php");
        return;
    }
    
} // end if post

$errors = $v->errors();
?><!doctype html>
<html lang="en">

<?php require_once __DIR__ . '/../inc/index_head.php'; ?>
<link rel="stylesheet" href="/css/passwordreset.css">
  
<body>
    
    <div id="wrapper">
        <?php require_once __DIR__ . '/../inc/header.php'; ?>
        <?php include __DIR__ . '/../inc/flash.inc.php'; ?>
        <h1>Password Reset</h1>
        
        <?php 
            if('GET' == filter_input(INPUT_SERVER, 'REQUEST_METHOD') &&
                 is_numeric(filter_input(INPUT_GET, 'key', FILTER_SANITIZE_NUMBER_INT)) && 
                !empty(filter_input(INPUT_GET, 'email', FILTER_SANITIZE_EMAIL))){

                

                // var_dump($_GET);
                echo "<h1>TEST</h1>";

                $email = filter_input(INPUT_GET, 'email', FILTER_SANITIZE_EMAIL);
                $key   = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_NUMBER_INT);
                $query = "select * from customer where email = :email and activation_key =:key";
                $stmt = $dbh->prepare($query);
                $params = array(
                    ':email' => $email,
                    ':key'  => $key,
                );
                $stmt->execute($params);
                $user = $stmt->fetch(PDO::FETCH_ASSOC);
                // Only if we find provided email in database
                if ($user) {
                    $customer_id = $user['customer_id'];
                    $_SESSION['user_id'] = $customer_id;
                    setFlash('success', 'Please using the following form to reset your password.');  
                    $passwordForm = true;
                }
                else{
                    setFlash('error', 'The information you provided is not valid, please request a new key or contact us.');                        
                }
            }
            ?>

        <div class="container">
                
                <!-- <form action="<?=filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_STRING)?>" method="post" class="password_reset_form">
                    <label for="email">Please input your email:</label><br>
                    <input type="text" id="email" name="email" placeholder="you_email@domain.com"><br>
                    <input type="submit" value="Submit" id="submit">
                </form> -->
            </div>
                      
            <?php if(isset($passwordForm) && $passwordForm === true): ?>    
                <div class="container">
                    <form action="<?=filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_STRING)?>" method="post" class="password_reset_form">
                        <label for="password">Please input your email:</label><br>
                        <input type="password" id="password" required="required" name="password" placeholder="Password"><br>
                        <label for="password_confirm">Confirm Password:</label><br>
                        <input type="password" id="password_confirm" required="required"  name="password_confirm" placeholder="Verify Password"><br>
                        
                        <input type="submit" value="Submit" id="submit">
                    </form>
                </div>
            <?php else: ?>          
                <div class="container">
                <form action="<?=filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_STRING)?>" method="post" class="password_reset_form">
                    <label for="email">Please input your email:</label><br>
                    <input type="text" id="email" required="required"  name="email" placeholder="you_email@domain.com"><br>
                    <input type="submit" value="Submit" id="submit">
                </form>
            <?php endif; ?> 

        <div class="break"></div>
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>