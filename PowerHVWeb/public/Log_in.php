<?php

use classes\Validator;

include __DIR__ . '/../config/config.php';


// Any page variables
$page = 'Log in';
// Any page includes

// If user clicked log out link, call log_out function
if (filter_input(INPUT_GET, 'logout')) {
    log_out();
    header('Location: Log_in.php');
    die;
}

$v = new Validator();

if ('POST' == filter_input(INPUT_SERVER, 'REQUEST_METHOD')) {
    // Required fields check
    $v->required('email');
    $v->required('password');
  
    if (!$v->errors()) {
        // Get stored password from database
        $query = "select * from customer where email = :email";
        $stmt = $dbh->prepare($query);
        $params = array(
        ':email' => filter_input(INPUT_POST, 'email')
        );
        $stmt->execute($params);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        // Only if we find provided email in database
        if ($user) {
            $form_password = filter_input(INPUT_POST, 'password');
            $stored_password = $user['pw'];
            // Compare form password with stored password, if they match
            if (password_verify($form_password, $stored_password)) {
                // Regenerate a session id to this authentic user
                session_regenerate_id();
                $_SESSION['logged_in'] = 1;
                // If the user is administrator
                if ($user['is_admin'] == 1) {
                    $_SESSION['is_admin'] = 1;
                    $customer_id = $user['customer_id'];
                    $_SESSION['user_id'] = $customer_id;
                    setFlash('success', 'Welcome Back, Administrator: '
                             . $user['first_name'] . '! You have successfully logged in');
                    // Redirect to dashboard page under admin folder
                    header('Location: /admin/index.php');
                    die;
                } else {// If the user is not administrator
                    setFlash('success', 'Welcome Back, ' . $user['first_name'] . '! You have successfully logged in');
                    // Store this user's customer_id in database, for use in profile.php
                    $customer_id = $user['customer_id'];
                    $_SESSION['user_id'] = $customer_id;
                    // Redirect to user profile page
                    header('Location: index.php');
                    die;
                }
            } else {// if they don't match
                setFlash('error', 'Password is not correct');
            } // end if password varified
        } else { // if doesn't find email address in database
            setFlash('error', 'Email doesn\'t exist');
        } //  end if has user
    } // end if no errors
} // end if post

$errors = $v->errors();
?><!DOCTYPE html>
<html lang="en">
<?php require_once __DIR__ . '/../inc/index_head.php'; ?>
<link rel="stylesheet" href="/css/log_in.css">
<style>
    form.log_in_form{
        margin-left: 280px;
        width: 400px;
    }
    
    legend{
        width:80px;
        font-size: 1.5rem;
    }
        
    form.log_in_form input{
        margin-top: 1rem;   
    }
    
    form.log_in_form label{
        margin-top: 10px;
        margin-left: 20px;
    }
    
    form.log_in_form p input#email,
    form.log_in_form p input#password{
        margin-top: 1rem;
        width: 200px;
        line-height: normal !important;
    }
    
    button#log_in_button{
        width: 150px;
        height: 30px;
        background-color: #3c88af;
        color: white;
        margin: 20px;
        font-size: 20px;
    }
    
    a#registration_link{
        display: inline-block;
        width: 150px;
        height: 30px;
        background-color: #3c88af;
        color: white;
        margin: 20px;
        font-size: 20px;
        padding-left: 30px;
    }

    div#passwordreset{
        text-align: center;
        font-size: 25px;
    }
    
</style>
<main>
    <?php require_once __DIR__ . '/../inc/header.php'; ?>
    <?php include __DIR__ . '/../inc/flash.inc.php'; ?>    
    <form method="post" action="<?=filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_STRING)?>" class="log_in_form" >
        <fieldset>
            <legend><?=$page?></legend>
            <!-- POST $_SESSION['csrf_token'] -->
            <input type="hidden" name="csrf_token" value="<?=getToken('csrf_token')?>" />
            <p>
                <label for="email">Email: </label>
                <input type="text" 
                       name="email" 
                       id="email" 
                       value="<?=clean_post('email')?>" /><!-- Sticky -->
                <?=(isset($errors['email']))
                    ? "<span class='error'>{$errors['email']}</span>" : '' ?><!-- Output Error information -->
            </p>
            <p>
                <label for="password">Password: </label>
                <input type="password" name="password" id="password" />
                <?=(isset($errors['password'])) ? "<span class='error'>{$errors['password']}</span>" : '' ?><br />
            </p>
            <p>
                <button id="log_in_button">Log in</button>
                <a id="registration_link" href="registration.php">Registration</a>
            </p>  
            <div id="passwordreset">
                <a  href="password_reset.php">Reset Password</a>
            </div> 
        </fieldset>    
    </form>  
</main>

<!-- Include footer.php -->
<?php require_once __DIR__ . '/../inc/modal.php'; ?>
<?php require_once __DIR__ . '/../inc/footer.php'; ?>
</html>