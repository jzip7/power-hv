<?php

include __DIR__ . '/../config/config.php';

// Any page variables
$page = 'Index';

// sign_out if
if (filter_input(INPUT_GET, 'logout')){
    unset($_SESSION['user']);
    unset($_SESSION['role']);
    unset($_SESSION['csrf_token']);
    unset($_SESSION['user_name']);
    unset($_SESSION['cart']);
    session_regenerate_id();
    setFlash('success', 'You have successfully logged out!');
    header('location:index.php');
    die;
}
     

?><!doctype html>
<html lang="en">

<?php require_once __DIR__ . '/../inc/index_head.php'; ?>
<!--link rel="stylesheet" href="/css/products.css"-->
  
<body>
    
    <div id="wrapper">
        <?php require_once __DIR__ . '/../inc/header.php'; ?>
        <?php include __DIR__ . '/../inc/flash.inc.php'; ?>
        <div class="main-banner">
            <div class="container">

                <div class="banner">                      
                    <div class="hero_img">                            
                        <img class="sensor" src="img/sensor.png" />
                    </div>  
                    <div class="banner_text">                            
                        <h2>Monitoring Sensors 
                        </h2>
                        <br/>
                        <br/>
                        <br/>
                        <br/>

                        <p>Track every single bushing and predict when a bushing is going to fail. 
                        </p>
                        <a class="more_button" href="/show.php?product_id=7">More</a>
                    </div>                            
                </div> 

                <div class="four_blocks">
                    <img class="certification" src="img/environmental-friendly.png" />
                    <div class="parameter">Installed  (kVAh) 95,300.52 </div>
                    <div class="parameter">CO2 reduced  (kT) 357.85 </div>
                    <div class="parameter">Water saved  (kL) 1.49 </div>
                </div>                
                
            </div>
        </div>
        

        <div class="container">
            <div class="twelve column">
                <div class="text-banner">
                    <div class="text-box-1">
                        <div>
                            <img src="img/energy_efficient_bushings.png">
                        </div>
                        <h3>Energy Efficient Bushings</h3>
                        <p>With our patented solution using magnetic decoupling, we increase efficiency of bushings by 80%, while keeping the same quality and life expectancy of other bushings availible today. This bushing is PCB free.</p> 
                        
                        <a class="more_button" href="/show.php?product_id=1">More</a>
                    </div>
                    <div class="text-box-2">
                        <div>
                            <img src="img/energy_efficient_bushings.png">
                        </div>
                        <h3>Regular Bushings</h3>
                        <p>With our advanced manufacturing processes, we are able to deliver these standard bushings in only 4 weeks, 3 times shorter than the industry average. This bushing is PCB free.</p>
                       
                        <a class="more_button" href="/show.php?product_id=2">More</a>
                    </div>
                    <div class="text-box-3">
                        <div>
                            <img src="img/sensor_2.png">
                        </div>
                        <h3>Monitoring Sensors</h3>
                        <p>Our Monitoring Sensors allow our customers to predict when their bushings are going to fail, preventing bushing and transformer failures therefore saving money and keeping the grid more stable.</p> 
                        
                        <a  class="more_button" href="/show.php?product_id=7">More</a>
                    </div>
                    <div class="text-box-4">
                        <div>
                            <img src="img/connectors.jpg">
                        </div>
                        <h3>Connectors</h3>
                        <div>
                            <img src="img/corona_rings.png">
                        </div>
                        <h3>Corona Rings</h3>                       
                        <a  class="more_button" href="/show.php?product_id=6">More</a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="twelve column">
                <div class="registration">
                    <div class="text">
                        <h3>We are a proud member of:</h3>
                    </div>
                    <div class="member">
                        <img src="img/cigre.png" alt="International Council on Large Electric Systems" title="International Council on Large Electric Systems">
                        <a title="The Canadian Federation of Independent Business"><img src="img/cfib.png" alt="The Canadian Federation of Independent Business" title="The Canadian Federation of Independent Business"></a>
                    </div>
                    <div class="member member2">
                        <div style="height: 100px;">
                            <img src="img/apegm.png" title="Association of Professional Engineers and Geoscientists Manitoba" alt="Association of Professional Engineers and Geoscientists Manitoba">
                        </div>
                        <img src="img/plug_and_play.png" title="Plug and Play Tech Center" alt="Plug and Play Tech Center">
                    </div>
                    <div class="member member3">
                        <div style="height: 90px;">
                            <img src="img/neia.png" alt="Newfoundland & Labrador Environmental Industry Association" title="Newfoundland & Labrador Environmental Industry Association">
                        </div>
                        <img src="img/meia.png" alt="Manitoba Environmental Industries Association" title="Manitoba Environmental Industries Association">
                    </div>
                    <div class="member">
                        <img src="img/clean_tech_open.png" title="The Cleantech Open" alt="The Cleantech Open">
                    </div>                    
                </div>
            </div>
        </div>
        
        <div class="break"></div>
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>