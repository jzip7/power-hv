<?php
include __DIR__ . '/../config/config.php';

if('POST' != $_SERVER['REQUEST_METHOD']) {
    header('location:index.php');
    die;
}

// csrf token validation
$input = new \classes\Input();
$tokenIsValid = $input->matchToken(filter_input(INPUT_POST,'token'));

if(!$tokenIsValid){
    die('wrong token');
}

if(isset($_POST['email']) && !isset($_POST['password'])){
    $data1 = [];
    if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
        $data['success'] = 'email_false';
    }else{
        $data['success'] = 'email_true';
    }
    header('Content-Type:application/json');
    echo json_encode($data);
    die;
}

if(isset($_POST['password'])){
    $data2 = [];
    if($_POST['password'] == ''){
        $data2['success'] = 'empty_password';
    }else{
        $query = 'select * from customer where email_address = :email';
        $params = array(':email' => $_POST['email']);
        $stmt = $dbh->prepare($query);
        $stmt->execute($params);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user){
            session_regenerate_id();
            if(!password_verify($_POST['password'], $user['password'])){
                $data2['success'] = 'wrong_password';
            }else{   
                // Regenerate a session id to this authentic user
                session_regenerate_id();
                $_SESSION['logged_in'] = 1;
                // If the user is administrator
                if ($user['is_admin'] == 1) {
                    $_SESSION['is_admin'] = 1;
                    $customer_id = $user['customer_id'];
                    $_SESSION['user_id'] = $customer_id;
                    setFlash('success', 'Welcome Back, Administrator: '
                             . $user['first_name'] . '! You have successfully logged in');
                    // Redirect to dashboard page under admin folder
                    $destination = '/admin/index.php';
                    //die;
                } else {// If the user is not administrator
                    setFlash('success', 'Welcome Back, ' . $user['first_name'] . '! You have successfully logged in');
                    // Store this user's customer_id in database, for use in profile.php
                    $customer_id = $user['customer_id'];
                    $_SESSION['user_id'] = $customer_id;
                    // Redirect to user profile page
                    $destination = '/index.php';
                    //die;
                }
                /*if(isset($user['user_id'])){
                    $_SESSION['user'] = $user['manager_id'];
                    $_SESSION['role'] = 'manager';
                    $_SESSION['user_name'] = $user['first_name'] . " " .$user['last_name'] ;

                    setFlash('success', 'You have successfully logged in');
                    $destination = '/index.php';
                    //$destination = 'about.php';
                }*/
                
                $data2['success'] = 'true';
                $data2['destination'] = $destination;

            }
        }else{
            $data2['success'] = false;
        }
        
    }
    header('Content-Type:application/json');
    echo json_encode($data2);
    die;
}