<?php

use classes\Validator;

// Assign a variable name for each of pages
$page = 'registration';
$style = 'pa';
$page_name = 'Registration | Power HV';

// Include header and navigation  inc.php
include __DIR__ . '/../config/config.php';
require __DIR__ . '/../classes/Validator.php';

$v = new Validator();

// Required fields: all fields
$required = ['email', 'company', 'first_name', 'last_name', 'position', 'phone', 'password', 'password_confirmation'];

// If REQUEST METHOD is POST
if ('POST' == $_SERVER['REQUEST_METHOD']) {
    foreach ($required as $key => $value) {
        // Required fields validation
        $v->required($value);
    }// end foreach
  
    // Email validation
    $v->email('email');
    
    // Only alphabet fields and no special chars field validation
    $v->alphabet('company');
    $v->alphabet('first_name');
    $v->alphabet('last_name');
    $v->alphabet('position');    

    // Phone validation
    $v->phone('phone');   

    // password complixty validation
    $v->passwordComplixty('password');
  
    // Password confirmation
    $v->passwordConfirmation('password', 'password_confirmation');

    $errors = $v->errors();
    
    // If no error, insert record into database
    if (!$errors) {
        try {
            // Create query
            $query = "INSERT INTO
                      customer
                      (first_name, last_name, position,
                      company, phone, email, pw, created_at, updated_at)
                      VALUES
                      (:first_name, :last_name, :position,
                      :company, :phone, :email, :pw, now(), now())";

            // Prepare query
            $stmt = $dbh->prepare($query);
      
            // Bind placeholders with inserted values
            $params = array(
                ':first_name' => clean_post('first_name'),
                ':last_name' => clean_post('last_name'),
                ':position' => clean_post('position'),
                ':company' => clean_post('company'),
                ':phone' => clean_post('phone'),
                ':email' => clean_post('email'),
                ':pw' => password_hash(clean_post('password'), PASSWORD_DEFAULT)
            );

            // execute query
            $stmt->execute($params);
      
            // Get last inserted record's PK
            $customer_id = $dbh->lastInsertId();
      
            $_SESSION['logged_in'] = 1;
            setFlash('success', 'Congratulations, ' . clean_post('first_name') . '! You have successfully registered.');
      
            // Store that PK into $_SESSION, for use in profile.php
            $_SESSION['user_id'] = $customer_id;

            // Redirect
            header('Location: profile.php?user_id=' . $customer_id);
            exit;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    } // end if no error
}// end IF POST

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../inc/common_head.php'; ?>

<style>
    .error{
         color: red;
         font-size: 1.6rem;
    }
    
    legend{
        width:80px;
        font-size: 1.5rem;
    }
        
    form.registration_form input{
        margin-top: 1rem;
        font-size: 20px;
    }
    
    form.registration_form label{
        margin-top: 20px;
        margin-left: 20px;
        float: left;
    }
    
    form.registration_form input#email,
    form.registration_form input#password{
        margin-top: 0.2rem;
        width: 200px;
    }
    
    button#registration_button{
        width: 150px;
        height: 30px;
        background-color: #3c88af;
        color: white;
        margin: 20px;
        font-size: 20px;
    }
</style>

<link rel="stylesheet" href="/css/sign_up.css">

<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../inc/header.php'; ?>
        <?php require_once __DIR__ . '/../inc/flash.inc.php'; ?>

        <div class="container">
            <div class="twelve column">
                <div class="heading">
                    <h2>Sign up</h2>
                    <p>Fill out the form to create your profile</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="twelve column">
                <div class="school-reg">

                    <form action="<?=$_SERVER['PHP_SELF']?>" method="post" autocomplete="on" novalidate class="registration_form">
    
                        <fieldset>
                            <legend>Registration</legend>
                            <input type="hidden" name="csrf_token" value="<?=getToken('csrf_token')?>" />
                            <input type="hidden" name="customer_id" 
                                   value="<?=clean_post('customer_id')?>" />

                            <!-- Foreach loop to output all fields-->
                            <?php foreach ($required as $key => $value) : ?>
                            <p>
                                <label for="<?=($value == 'phone') ? 'phone1' : $value ?>">
                                    <?=($value == 'phone') ? 'Phone' : label($value) ?><span>*</span>
                                </label>          
                                <input type="<?=($value == 'password' ||
                                                 $value == 'password_confirmation') ? 'password' : 'text' ?>"
                                       name="<?=$value?>" 
                                       id="<?=($value == 'phone') ? 'phone1' : $value ?>"
                                       value="<?=($value!='password_confirmation')? clean_post($value) : '' ?>" />
                                       <!--Sticky except password confirmation field -->  
                                <!-- Check if output error message -->
                                <?=(isset($errors[$value])) ? "<span class='error'>{$errors[$value]}</span>" : '' ?> 
                            </p>
                            <?php endforeach?>      

                            <p>
                                <button id="registration_button">Submit</button>
                            </p>

                        </fieldset>              

                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="required-message">
                    <p><span>*</span> Indicates required field</p>
                </div>
            </div>
        </div>
        <div class="break"></div>
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>
