<?php

use classes\Validator;

// Page variables
$page = 'Contact';
$style = '../css/contact';
$page_name = 'Contact us | Power HV';

// Include header and navigation  inc.php
include __DIR__ . '/../config/config.php';
require __DIR__ . '/../classes/Validator.php';

$v = new Validator();

// Required fields: all fields
$required = ['email', 'company', 'name', 'message'];

// If REQUEST METHOD is POST
if ('POST' == $_SERVER['REQUEST_METHOD']) {
    
    foreach ($required as $key => $value) {
        // Required fields validation
        $v->required($value);
    }// end foreach
  
    // Email validation
    $v->email('email');
    
    // Only alphabet fields and no special chars field validation
    //$v->alphabet('company');
    //$v->alphabet('name');   
    
    $errors = $v->errors();
    //echo "<pre>"; print_r($errors); echo "</pre>";
    // If no error, insert record into database
    if (!$errors) {
        echo "<pre>"; print_r('adkjflasdjfklas'); echo "</pre>";
        
        try {
            // Create query
            $query = "INSERT INTO
                      contact
                      (email, company, name, message, created_at, updated_at)
                      VALUES
                      (:email, :company, :name, :message, now(), now())";

            // Prepare query
            $stmt = $dbh->prepare($query);
      
            // Bind placeholders with inserted values
            $params = array(
                ':email' => clean_post('email'),
                ':company' => clean_post('company'),
                ':name' => clean_post('name'),
                ':message' => clean_post('message')
            );

            // execute query
            $stmt->execute($params);
         
            setFlash('success', 'Congratulations, ' . clean_post('name') . '! You have successfully sent the message to Power HV.');

            // Redirect
            header('Location: contact.php');
            exit; 
        } catch (Exception $e) {
            die($e->getMessage());
        }
    } // end if no error
}// end IF POST


?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../inc/common_head.php'; ?>
<body>
    <?php require_once __DIR__ . '/../inc/header.php'; ?>
    <?php include __DIR__ . '/../inc/flash.inc.php'; ?>
    <div id="wrapper">
        <div class="master-heading">
            <div class="container">
                <div class="twelve column">
                    <h2><i class="fas fa-envelope-open-text"></i> Contact us</h2>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="contact">
                    <h3>Message form</h3>
                    <form id="contact-form" method="post" action="<?=$_SERVER['PHP_SELF']?>" autocomplete="on" >
                        <input type="hidden" name="csrf_token" value="<?=getToken('csrf_token')?>" />
                        <div class="fields">
                            
                            <div class="field">
                                <label for="email">Email <span>*</span></label>
                                <input type="email" name="email" maxlength="50"
                                    title="Email is required" required>
                                <!-- <span class="input-error">Error message</span> -->
                            </div>
                            
                            <div class="field">
                                <label for="company">Company <span>*</span></label>
                                <input type="text" name="company" maxlength="50"
                                    title="Company is required" required>
                                <!-- <span class="input-error">Error message</span> -->
                            </div>                            
                            
                            <div class="field">
                                <label for="first-name">Name <span>*</span></label>
                                <input type="text" name="name" maxlength="50"
                                    title="Name is required" required>
                                <!-- <span class="input-error">Error message</span> -->
                            </div>
                            
                            <div class="field">
                                <label for="message">Message <span>*</span></label>
                                <textarea name="message" maxlength="1000" title="Message is required" required></textarea>
                                <!-- <span class="input-error">Error message</span> -->
                            </div>
                        </div>
                        <input type="submit" id="contact-btn" value="Send">
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="required-message">
                    <p><span>*</span> Indicates required field</p>
                </div>
            </div>
        </div>
        <div class="break"></div>
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>
