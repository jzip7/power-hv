<?php

// Assign a variable name for each of pages
$title = 'show_log';

// this starts session and sets error reporting
include __DIR__ . '/../../includes/header.inc.php';
include __DIR__ . '/../../includes/navigation.inc.php';

// If user doesn't log in or user is not a administrator
if (empty($_SESSION['logged_in']) || empty($_SESSION['is_admin'])) {
    setFlash('error', 'You must be logged as administrator to see the log record page');
    header('Location: Log_in.php');
    die;
}

// create select query
$query = "SELECT * FROM events order by id desc limit 10";

// execute the query
$stmt = $dbh->query($query);

// get the results
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

?><main>

    <?php include __DIR__ . '/../../lib/flash.inc.php'?>
    <h1>Log Records:</h1>

    <?php if ($result) : ?> 
    <!-- Foreach loop to output results -->
    <table>
        <tr>
            <th>Id</th>
            <th>Event</th>
        </tr>
        <?php foreach ($result as $key => $value) : ?>
        <tr>
            <!-- output the associative array -->
            <td><strong><?=$value['id']?></strong></td>
            <td><?=$value['event']?></td>            
        </tr>
        <?php endforeach; ?>    

    </table>
    <h2><a class="back_to" href="index.php">Back to home page</a></h2>
  
    <?php else : ?>
    <h2>Sorry there was a problem for registration</h2>

    <?php endif; ?>
</main>
<!-- Include footer.php -->
<?php include __DIR__ . '/../../includes/footer.inc.php'; ?>
