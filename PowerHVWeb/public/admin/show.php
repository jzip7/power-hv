<?php

// Assign a variable name for each of pages
$title = 'Show';

// this starts session and sets error reporting
include __DIR__ . '/../../includes/header.inc.php';
include __DIR__ . '/../../includes/navigation.inc.php';

// If medicine_id in URL is not empty
if (!empty(clean_get('medicine_id'))) {
    $id = intval(clean_get('medicine_id'));
}

$query = "SELECT medicine.*,
          functions.name as function
          FROM 
          medicine 
          JOIN functions USING(function_id)
		      WHERE 
          medicine_id = :medicine_id";

// prepare the query
$stmt = $dbh->prepare($query);

// Prepare params array
$params = array(
    ':medicine_id' => $id
);

// execute the query
$stmt->execute($params);

// get the result
$result = $stmt->fetch(PDO::FETCH_ASSOC);

?><main>
    <?php include __DIR__ . '/../../lib/flash.inc.php'?>
    <h1><?=$result['name']?></h1>
    <?php if ($result) : ?> 
    <!-- Foreach loop to output result -->
    <table>       
        <tr>
            <td><img src="/../imgs/<?=$result['image']?>" alt="<?=$result['name']?>"></td>            
            <td>
                <?php foreach ($result as $key => $value) : ?>
                    <ul>
                        <li><strong><?=label($key)?>:</strong> <?=$value?></li>                        
                    </ul>                    
                <?php endforeach; ?>
                <ul>
                    <li><strong><a href="edit.php?medicine_id=<?=$_GET['medicine_id']?>" 
                                   class="back_to">Edit</a></strong></li>
                    
                    <li>
                        <strong>
                            <a href="delete.php?medicine_id=<?=$_GET['medicine_id']?>&name=<?=reverseLabel($result['name'])?>" 
                                   class="back_to">Delete
                            </a>
                        </strong></li>
                </ul>                
            </td>
        </tr>
    </table>
    <h2><a class="back_to" href="Medicine.php">Back to Medicine page</a></h2>
  
    <?php else : ?>
    <h2>Sorry there was a problem for registration</h2>

    <?php endif; ?>
</main>
<!-- Include footer.php -->
<?php include __DIR__ . '/../../includes/footer.inc.php'; ?>
