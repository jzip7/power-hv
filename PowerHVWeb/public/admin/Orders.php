<?php

// Assign a variable name for each of 5 main pages
$title = 'Orders';

// Include header and navigation  inc.php
include __DIR__ . '/../../includes/header.inc.php';
include __DIR__ . '/../../includes/navigation.inc.php';


$query = 'SELECT orders.* 
          
          FROM
          orders          
          ORDER BY orders.order_id ASC';
$stmt = $dbh->query($query);
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
$array = array('order_id', 'customer_id','tax','sub_total','total_price',
               'payment_status','credit_card_number', 'security_code','created_at');

?><main> 
    <?php include __DIR__ . '/../../lib/flash.inc.php'?>
  
    <table class="box-table">
        <tr>
            <?php foreach ($array as $key => $value) :?>
            <th><?=label($value)?></th>
            <?php endforeach; ?>
        </tr>
        <?php foreach ($results as $row) : ?>
        <tr>
            <?php foreach ($array as $key => $value) :?>
            <td><?=$row[$value]?></td>
            <?php endforeach; ?>            
        </tr>
        <?php endforeach; ?>
    </table>
</main>

<!-- Include footer.php -->
<?php include __DIR__ . '/../../includes/footer.inc.php'; ?>