<?php

include __DIR__ . '/../../config/config.php';
use classes\Validator;
$v = new Validator();
// sign_out if
if (filter_input(INPUT_GET, 'logout')){
    unset($_SESSION['user']);
    unset($_SESSION['role']);
    unset($_SESSION['csrf_token']);
    unset($_SESSION['user_name']);
    unset($_SESSION['cart']);
    session_regenerate_id();
    setFlash('success', 'You have successfully logged out!');
    header('location:index.php');
    die;
}

// HOW would a user be valided as an admin??
//
$user_is_admin = true;
if ($user_is_admin)
{
    if ('POST' == filter_input(INPUT_SERVER, 'REQUEST_METHOD')) {

        if(!empty(filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_NUMBER_INT))){        
            // Required fields check
            $v->required('user_id');
            
            if (!$v->errors()) {
                try{
                    // Get stored password from database
                    $query = "update customer set is_deleted = 1 where customer_id = :customer_id";
                    $stmt = $dbh->prepare($query);
                    $params = array(
                    ':customer_id' => filter_input(INPUT_POST, 'user_id')
                    );
                    $stmt->execute($params);
                    setFlash('success', 'user has been deleted.');
                    }catch(Exception $e){
                        setFlash('error', 'Database error');
    
                    }
                } //  end if has user
            } // end if no errors
            // if ('POST' == filter_input(INPUT_SERVER, 'REQUEST_METHOD')) {
            //     var_dump($_POST);
            // }
         header('Location: ' . $_SERVER['HTTP_REFERER']);
        }

}
