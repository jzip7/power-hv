<?php
$title = 'Search';

include __DIR__ . '/../../includes/header.inc.php';
include __DIR__ . '/../../includes/navigation.inc.php';
include __DIR__ . '/../../classes/Validator.php';

try {
    if (!empty(clean_get('s'))) {
        // we get a search content
        $query = 'SELECT
                  medicine.*,
                  functions.name as function				
                  FROM
                  medicine
                  JOIN functions USING(function_id)				
                  WHERE
                  medicine.name LIKE :s
                  ORDER by medicine.medicine_id';

        $params = array(
            ':s' => "%{$_GET['s']}%"
        );
    } else {
        // create query
        $query = 'SELECT
                  medicine.*,
                  functions.name as function	
                  FROM
                  medicine
                  JOIN functions USING(function_id)	
                  ORDER by medicine.medicine_id';

        $params = [];
    } // end GET s

    // create statement
    $stmt = $dbh->prepare($query);

    $stmt->execute($params);

    // fetch our results
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

// end try
} catch (Exception $e) {
    echo $e->getMessage();
    die;
}

?><main>

    <!-- Only show this block if there is a search -->
    <?php if (!empty(clean_get('s'))) : ?>
    <h1>Your search for <span class="search"><?=clean_get('s')?></span> returned <?=count($results)?> result(s)</h1>
    <table>
        <?php foreach ($results as $row) : ?>
        <tr>
            <td>                
                <a href="show.php?medicine_id=<?=$row['medicine_id']?>">
                <img src="/../imgs/<?=$row['image']?>" alt="<?=$row['name']?>">
                </a>
            </td>            
                        
            <td>                   
                <ul>
                    <li><strong>Name:</strong> <?=$row['name']?></li>
                    <li><strong>Price:</strong> <?=$row['price']?></li>
                    <li><strong>Weight:</strong> <?=$row['weight']?></li>
                    <li><strong>Function:</strong> <?=$row['function']?></li>
                    <li><strong>Remaining Stock:</strong> <?=$row['remaining_stock']?></li>
                    <li><strong>Short Description:</strong> <?=$row['short_description']?></li>
                </ul>                      
            </td>
        </tr>
        <?php endforeach; ?> 
    </table>
    <h2><a class="back_to" href="Medicine.php">Back to Medicine page</a></h2>
  
    <?php else : ?>
    <h1>Please input something to search</h1>
    <?php endif; ?>
</main>
<!-- Include footer.php -->
<?php include __DIR__ . '/../../includes/footer.inc.php'; ?>
