<?php

// Assign a variable name for each of 5 main pages
$title = 'Delete';

// this starts session and sets error reporting
include __DIR__ . '/../../includes/header.inc.php';

// If medicine_id in URL is empty
if (!empty(clean_get('medicine_id'))) {
    $id = intval(clean_get('medicine_id'));
    $name = clean_get('name');
}

$query1 = "insert into medicine_backup select * from medicine where medicine_id = :medicine_id";
$query2 = "alter table medicine_backup add updated_at datetime default null";
$query3 = "update medicine_backup set updated_at = now() where medicine_id = :medicine_id";
$query4 = "delete from medicine where medicine_id = :medicine_id";

// prepare the query
$stmt = $dbh->prepare($query1);
// this query2 only need execute once
// $stmt = $dbh->prepare($query2);
// Prepare params array
$params = array(
    ':medicine_id' => $id
);

// execute the query
$stmt->execute($params);
$stmt = $dbh->prepare($query3);
// Prepare params array
$params = array(
    ':medicine_id' => $id
);

// execute the query
$stmt->execute($params);
$stmt = $dbh->prepare($query4);


// Prepare params array
$params = array(
    ':medicine_id' => $id
);

// execute the query
$stmt->execute($params);

setFlash('success', $name . ' has been deleted successfully.');
            
header('Location: Medicine.php');
die;
