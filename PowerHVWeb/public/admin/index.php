<?php

include __DIR__ . '/../../config/config.php';

// Any page variables
$page = 'Index';

// sign_out if
/*
if (filter_input(INPUT_GET, 'logout')){
    unset($_SESSION['user']);
    unset($_SESSION['role']);
    unset($_SESSION['csrf_token']);
    unset($_SESSION['user_name']);
    unset($_SESSION['cart']);
    session_regenerate_id();
    setFlash('success', 'You have successfully logged out!');
    header('location:index.php');
    die;
}*/

$query = 'SELECT * FROM customer';

$stmt = $dbh->query($query);

$contacts = $stmt->fetchAll(PDO::FETCH_ASSOC);

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../../inc/index_head.php'; ?>
<link href="/../css/index.css" rel="stylesheet" media="all">
<style>
    
    .box-table
    {
        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;	
        width: 100%;
        border-collapse: collapse;
        word-wrap:break-word; 
    }
    .box-table th
    {
        text-align: center;
        padding: 0;
        font-size: 20px;
        height: 50px;
        font-weight: normal;	
        background: #b9c9fe;
        border-top: 4px solid #aabcfe;
        border-bottom: 1px solid #fff;
        color: #039;
    }
    .box-table td
    {
        height: 50px;
        padding: 0;
        text-align: center;
        font-size: 20px;	
        background: #e8edff; 
        border-bottom: 1px solid #fff;
        color: #669;
        border-top: 1px solid transparent;
    }
    .box-table tr:hover td
    {
        background: #d0dafd;
        color: #339;
    }
    .details{
        display:none;
    }
    .hidden{
        display:none;
    }
</style>
<body>
    
    <div id="wrapper">
        <?php require_once __DIR__ . '/../../inc/header.php'; ?>
        <?php include __DIR__ . '/../../inc/flash.inc.php'?>
     
        <div class="container">
            <table class="box-table">
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th class="details">Company</th>
                    <th class="details">Position</th>
                    <th class="details">Phone</th>
                    <th>Email</th>
                    <th>Deleted</th>
                    <th class="details">Created At</th>
                    <th class="details">Updated At</th>
                    <th></th>
                </tr>
                <?php foreach ($contacts as $row) : ?>
                <tr>
                    <td><?=$row['customer_id']?></td>
                    <td><?=$row['first_name']?></td>
                    <td><?=$row['last_name']?></td>
                    <td class="details"><?=$row['company']?></td>
                    <td class="details"><?=$row['position']?></td>
                    <td class="details"><?=$row['phone']?></td>
                    <td><?=$row['email']?></td>
                    <td><?=$row['is_deleted']?></td>
                    <td class="details"><?=$row['created_at']?></td>
                    <td class="details"><?=$row['updated_at']?></td>
                    <td> <form action="user_del.php" method="post"> <input type="hidden" name="user_id" value="<?=$row['customer_id']?>"><input type="submit" value="Delete"> </form></td>
                </tr>
                <?php endforeach; ?>
            </table>                
        </div>
       
    </div>

    <?php require_once __DIR__ . '/../../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../../inc/footer.php'; ?>
</body>
</html>