<?php

use classes\Validator;

// Assign a variable name for each of pages
$title = 'Add';

// this starts session and sets error reporting
include __DIR__ . '/../../includes/header.inc.php';
include __DIR__ . '/../../includes/navigation.inc.php';
include __DIR__ . '/../../classes/Validator.php';

$v = new Validator();

// Required fields: all fields
$required = ['name', 'cost', 'price', 'weight', 'function_id',
             'short_description', 'long_description', 'remaining_stock', 'available'];

// If REQUEST METHOD is POST
if ('POST' == $_SERVER['REQUEST_METHOD']) {
    foreach ($required as $key => $value) {
        // Required fields validation
        $v->required($value);
    }// end foreach
    
    $v->fileRequired('imagefile');
    
    // Only alphabet fields and no special chars field validation
    $v->alphabet('name');
    $v->costAndPrice('cost');
    $v->costAndPrice('price');
    $v->costAndPrice('weight');
    $v->functionID('function_id');
    $v->remainingStock('remaining_stock');
    $v->availableBoolean('available');
      
    $errors = $v->errors();

    if (!$errors) {
        try {// Insert query
            $query = "Insert into 
                      medicine
                      (name, cost, price, weight, function_id, short_description,
                      long_description, remaining_stock, available, image)
                      values
                      (:name, :cost, :price, :weight, :function_id, :short_description,
                      :long_description, :remaining_stock, :available, :image)";

            // Prepare query
            $stmt = $dbh->prepare($query);

            // Bind placeholders with inserted values
            $params = array(
                ':name' => clean_post('name'),
                ':cost' => clean_post('cost'),
                ':price' => clean_post('price'),
                ':weight' => clean_post('weight'),
                ':function_id' => clean_post('function_id'),
                ':short_description' => clean_post('short_description'),
                ':long_description' => clean_post('long_description'),
                ':remaining_stock' => clean_post('remaining_stock'),
                ':available' => clean_post('available'),
                ':image' => ($_FILES['imagefile']['name'] == '') ? clean_post('image') : $_FILES['imagefile']['name']
            );

            // execute query
            $stmt->execute($params);

            setFlash('success', clean_post('name') . ' has been added successfully.');
            
            header('Location: Medicine.php');
            die;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    } // end if no error
}// end IF POST


?><main>

<div class="container">    

<h1><?=$title?></h1>

<form action="<?=$_SERVER['PHP_SELF']?>" 
      method="post" 
      enctype="multipart/form-data">
    
    <?php foreach ($required as $key => $value) : ?>
        <?php if ($value != 'long_description' &&
              $value != 'short_description') : ?>
    <p>
        <label for="<?=($value == 'name') ? 'name1' : $value ?>"><?=label($value)?></label>          
        <input type="text"
               name="<?=$value?>" 
               id="<?=($value == 'name') ? 'name1' : $value ?>"
               value="<?=clean_post($value)?>" /> <!--Sticky fields -->  
        <!-- Check if output error message -->
        <?=(isset($errors[$value])) ? "<span class='error'>{$errors[$value]}</span>" : '' ?> 
    </p>
        <?php endif; ?>
    <?php endforeach;?>     
    
    <p> 
        <label for="short_description">Short Description</label><br />
        <textarea style="width: 50%; height: 100px;" 
                  name="short_description" 
                  id="short_description"><?=clean_post('short_description')?></textarea>
        <?=(isset($errors['short_description'])) ? "<span class='error'>{$errors['short_description']}</span>" : '' ?> 
    </p>
    
    <p>
        <label for="long_description">Long Description</label><br />
        <textarea style="width: 50%; height: 100px;" 
                  name="long_description" 
                  id="long_description"><?=clean_post('long_description')?></textarea>
        <?=(isset($errors['long_description'])) ? "<span class='error'>{$errors['long_description']}</span>" : '' ?> 
    </p>
    
    <p>
        <label for="imagefile">Image</label>
        <input type="file" name="imagefile" id="imagefile">
        <?=(isset($errors['imagefile'])) ? "<span class='error'>{$errors['imagefile']}</span>" : '' ?>    
    </p>

    <p><button class="some_buttons">Add Medicine</button>

</form>

</div>

</main>

<!-- Include footer.php -->
<?php include __DIR__ . '/../../includes/footer.inc.php'; ?>
