<?php

use classes\Validator;

// Assign a variable name for each of pages
$title = 'Edit';

// this starts session and sets error reporting
include __DIR__ . '/../../includes/header.inc.php';
include __DIR__ . '/../../includes/navigation.inc.php';
include __DIR__ . '/../../classes/Validator.php';

// If medicine_id in URL is not empty
if (!empty(clean_get('medicine_id'))) {
    $id = intval(clean_get('medicine_id'));
} else {// If medicine_id in URL is empty, get from hidden field
    $id = intval(clean_post('medicine_id'));
}

$query = "SELECT medicine.*,
          functions.name as function
          FROM 
          medicine 
          JOIN functions USING(function_id)
          WHERE 
          medicine_id = :medicine_id";

// prepare the query
$stmt = $dbh->prepare($query);

// Prepare params array
$params = array(
    ':medicine_id' => $id
);

// execute the query
$stmt->execute($params);

// get the result
$medicine = $stmt->fetch(PDO::FETCH_ASSOC);

$v = new Validator();

// Required fields: all fields
$required = ['name', 'cost', 'price', 'weight', 'function',
             'short_description', 'long_description', 'remaining_stock', 'available'];

// If REQUEST METHOD is POST
if ('POST' == $_SERVER['REQUEST_METHOD']) {
    foreach ($required as $key => $value) {
        // Required fields validation
        $v->required($value);
    }// end foreach
  
    // Only alphabet fields and no special chars field validation
    $v->alphabet('name');
    $v->costAndPrice('cost');
    $v->costAndPrice('price');
    $v->costAndPrice('weight');
    $v->functionID('function_id');
    $v->remainingStock('remaining_stock');
    $v->availableBoolean('available');
      
    $errors = $v->errors();

    if (!$errors) {
        try {// Update query
            $query = "update 
                      medicine
                      set
                      name = :name,
                      cost = :cost,
                      price = :price,
                      weight = :weight,
                      function_id = :function_id,
                      short_description = :short_description,
                      long_description = :long_description,
                      remaining_stock = :remaining_stock,
                      available = :available,
                      image = :image,
                      updated_at = now()
                      where
                      medicine_id = {$id}";

            // Prepare query
            $stmt = $dbh->prepare($query);

            // Bind placeholders with inserted values
            $params = array(
                ':name' => clean_post('name'),
                ':cost' => clean_post('cost'),
                ':price' => clean_post('price'),
                ':weight' => clean_post('weight'),
                ':function_id' => clean_post('function_id'),
                ':short_description' => clean_post('short_description'),
                ':long_description' => clean_post('long_description'),
                ':remaining_stock' => clean_post('remaining_stock'),
                ':available' => clean_post('available'),
                ':image' => ($_FILES['imagefile']['name'] == '') ? clean_post('image') : $_FILES['imagefile']['name']
            );

            // execute query
            $stmt->execute($params);

            setFlash('success', clean_post('name') . ' has been updated successfully.');
            
            header('Location: show.php?medicine_id=' . $id);
            die;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    } // end if no error
}// end IF POST

?><main>
<?php include __DIR__ . '/../../lib/flash.inc.php'?>
<div class="container">    

<h1><?=$title?></h1>

<form action="<?=$_SERVER['PHP_SELF']?>" 
      method="post" 
      enctype="multipart/form-data">
    
    <?php foreach ($medicine as $key => $value) : ?>
        <?php if ($key != 'long_description' &&
                  $key != 'short_description' &&
                  $key != 'medicine_id' &&
                  $key != 'created_at' &&
                  $key != 'updated_at') : ?>
    <p>
        <label for="<?=($key == 'name') ? 'name2' : $key ?>"><?=label($key)?></label>          
        <input type="text"
               name="<?=$key?>" 
               id="<?=($key == 'name') ? 'name2' : $key ?>"
               value="<?=(!empty(clean_get('medicine_id')))? $value : clean_post($key)?>" /> <!--Sticky field -->  
        <!-- Check if output error message -->
        <?=(isset($errors[$key])) ? "<span class='error'>{$errors[$key]}</span>" : '' ?> 
    </p>
        <?php endif; ?>
    <?php endforeach;?>     
    
    <p> 
        <label for="short_description">Short Description</label><br />
        <textarea style="width: 50%; height: 100px;" 
                  name="short_description" 
                  id="short_description"><?=$medicine['short_description']?></textarea>
        <?=(isset($errors[$key])) ? "<span class='error'>{$errors[$key]}</span>" : '' ?> 
    </p>
    
    <p>
        <label for="long_description">Long Description</label><br />
        <textarea style="width: 50%; height: 100px;" 
                  name="long_description" 
                  id="long_description"><?=$medicine['long_description']?></textarea>
        <?=(isset($errors[$key])) ? "<span class='error'>{$errors[$key]}</span>" : '' ?> 
    </p>
    
    <p>
        <label for="imagefile">Image</label><br />
        <input type="file" name="imagefile" id="imagefile"><br />
        <?=(isset($errors[$key])) ? "<span class='error'>{$errors[$key]}</span>" : '' ?> 
        <img style="width: 150px; height: 100px"
             src="/imgs/<?=e_attr($medicine['image'])?>" 
             alt="<?=e_attr($medicine['image'])?>" />
    </p>
    
    <input name="medicine_id" type="hidden"
           value="<?=e_attr($medicine['medicine_id'])?>" />

    <p><button class="some_buttons">Update Record</button>

</form>

</div>

</main>

<!-- Include footer.php -->
<?php include __DIR__ . '/../../includes/footer.inc.php'; ?>
