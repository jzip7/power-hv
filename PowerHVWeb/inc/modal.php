<?php 
    $input = new \classes\Input();
    $input->setToken();
/*
use classes\Validator;

// Assign a variable name for each of pages
$title = 'Log in';

include __DIR__ . '/../includes/header.inc.php';
include __DIR__ . '/../includes/navigation.inc.php';
include __DIR__ . '/../classes/Validator.php';


// If user clicked log out link, call log_out function
if (filter_input(INPUT_GET, 'logout')) {
    log_out();
    header('Location: Log_in.php');
    die;
}

$v = new Validator();

if ('POST' == filter_input(INPUT_SERVER, 'REQUEST_METHOD')) {
    // Required fields check
    $v->required('email');
    $v->required('password');
  
    if (!$v->errors()) {
        // Get stored password from database
        $query = "select * from customer where email = :email";
        $stmt = $dbh->prepare($query);
        $params = array(
        ':email' => filter_input(INPUT_POST, 'email')
        );
        $stmt->execute($params);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        // Only if we find provided email in database
        if ($user) {
            $form_password = filter_input(INPUT_POST, 'password');
            $stored_password = $user['pw'];
            // Compare form password with stored password, if they match
            if (password_verify($form_password, $stored_password)) {
                // Regenerate a session id to this authentic user
                session_regenerate_id();
                $_SESSION['logged_in'] = 1;
                // If the user is administrator
                if ($user['is_admin'] == 1) {
                    $_SESSION['is_admin'] = 1;
                    $customer_id = $user['customer_id'];
                    $_SESSION['user_id'] = $customer_id;
                    setFlash('success', 'Welcome Back, Administrator: '
                             . $user['first_name'] . '! You have successfully logged in');
                    // Redirect to dashboard page under admin folder
                    header('Location: /admin/index.php');
                    die;
                } else {// If the user is not administrator
                    setFlash('success', 'Welcome Back, ' . $user['first_name'] . '! You have successfully logged in');
                    // Store this user's customer_id in database, for use in profile.php
                    $customer_id = $user['customer_id'];
                    $_SESSION['user_id'] = $customer_id;
                    // Redirect to user profile page
                    header('Location: index.php');
                    die;
                }
            } else {// if they don't match
                setFlash('error', 'Password is not correct');
            } // end if password varified
        } else { // if doesn't find email address in database
            setFlash('error', 'Email doesn\'t exist');
        } //  end if has user
    } // end if no errors
} // end if post

$errors = $v->errors();
*/

?>
<div id="login-frame">
   <div id="login-align">
      <div id="login-title">
          <h2>Sign in</h2>
          <p>Enter your credentials to login</p>
      </div>
      <form id="login-form" method="post" action="<?=filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_STRING)?>" 
        novalidate>
         <input type="hidden" id="token" name="token" value="<?=$input->getToken()?>">
         <div class="field">
             <label for="email">Email</label>
             <input type="email" name="email" id="email" placeholder="" 
                title="Username is required" required>
             <span class="input-error" id="email_error_mess" style="visibility:hidden;"></span>
        </div>
        <div class="field">
            <label for="email">Password</label>
            <input type="password" name="password" id="password" placeholder="" 
                title="Password is required" required>
            <span class="input-error" id="pass_error_mess" style="visibility:hidden;"></span>
        </div>
        <input type="submit" id="signin-btn" value="Sign in">
      </form>
      <div class="forgot">
         
         <a href="registration.php">Create a new account?</a>
      </div>
   </div>
</div>

<div id="overlay"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        
        $('#email').change(function(e){
            e.preventDefault();
            var email = $('#email').val();
            var token = $('#token').val();
            var data = {};
            data.token = token;
            data.email = email;
            /*console.log(email);*/
            $.post('validate.php', data, function(response){
                /*console.log(response);*/
                if(response.success == 'email_false'){
                    $('#email_error_mess').css('visibility','visible');
                    $('#email_error_mess').text('Please enter valid Email Address');
                    $("#signin-btn").attr('disabled',true);
                    $("#signin-btn").css('background-color', '#eee');
                    $('#pass_error_mess').css('visibility','hidden');
                }else{
                    $('#email_error_mess').css('visibility', 'hidden');
                    $("#signin-btn").attr('disabled', false);
                    $("#signin-btn").css('background-color', '#449b3c');
                    $("#signin-btn").hover(function(){
                        $(this).css('background-color', '#aa1229');
                    },function(){
                         $(this).css('background-color', '#449b3c');
                    });
                }
                
            });
        }); 
        $("#signin-btn").click(function(e){
            e.preventDefault();
            var password = $('#password').val();
            var email = $('#email').val();
            var token = $('#token').val();
            var data = {};
            data.token = token;
            data.password = password;
            data.email = email;
            $.post('validate.php', data, function(response){
                /*console.log(response);*/
                if(response.success == 'wrong_password'){
                    $('#pass_error_mess').css('visibility','visible');
                    $('#pass_error_mess').text('There was a problem with your credentials');
                }else if(response.success == 'empty_password'){
                    $('#pass_error_mess').css('visibility','visible');
                    $('#pass_error_mess').text('Password cannot be empty');
                }else{
                    $('#pass_error_mess').css('visibility','hidden');
                }
                
                if(response.success == 'true'){
                    window.location = response.destination;
                }
                
            });
        });
    }); 
</script>