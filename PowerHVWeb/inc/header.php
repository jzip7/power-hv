<header id="top">
   <div class="container">
      <div class="twelve column">
         <?php
            $navItems = array(
                'Products' => 'products.php',
                'My Sensors' => 'sensors.php',
                'About' => 'about.php',
                'Contact' => 'contact.php',
            );
        ?>
        <div id="logo">
           <a href="index.php" title="Home"><img src="/../img/logo.png"></a>
        </div>
         <nav>
            <ul>
                <?php if (empty($_SESSION['is_admin'])) : ?>
                <?php foreach ($navItems as $key => $value): ?>
                    <li><a class="<?= $page == $key ? 'highlight' : '' ?>" title="<?= $key ?>"
                     href="<?= $value ?>"><?= $key ?></a></li>
                <?php endforeach; ?>
                <?php else : ?>
                    <li><a title="Administrator Dashboard" href="#">Administrator Dashboard</a></li>
                <?php endif; ?>
            </ul>
         </nav>
          <?php if (empty($_SESSION['logged_in'])) : ?>
            <a href="Log_in.php">
         <div id="signin" class="">
             <p title="Sign in"><i class="fas fa-user"></i>Sign in</p>
         </div>
          </a>
            <?php elseif (empty($_SESSION['is_admin'])) : ?><!-- The user is not admin -->
            
            <div class="utility" id="profile"><a href="/../Log_in.php?logout=1">Log out</a><a href="/../profile.php">Profile</a></div> 
            <?php else : ?><!-- The user is administrator -->
            <div class="utility" id="profile"><a href="/../Log_in.php?logout=1">Log out</a></div>
            <!--div class="utility" id="profile"><a href="/../admin/show_log.php">LogReco</a></div-->
            <?php endif; ?>
          
          
          
         <div id="mobile-nav">
            <ul>
                <li><a href="Log_in.php"><i class="fas fa-user user"></i></a></li>
                <li><i class="fas fa-bars bars" id="hamburger"></i></li>
            </ul>
         </div>

         <div id="mobile-menu">
            <ul>
              <li><a href="products.php">Products</a></li>
              <li><a href="sensors.php">My Sensors</a></li>
              <li><a href="about.php">About</a></li>
              <li><a href="contact.php">Contact</a></li>
            </ul>
         </div>

      </div>
   </div>
</header>
