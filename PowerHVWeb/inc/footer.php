<footer>
   <div class="container">
      <div class="twelve column">
         <?php
            $navItems = array(
                'Products' => 'products.php',
                'My Sensors' => 'sensors.php',
                'About' => 'about.php',
                'Contact' => 'contact.php'
            );
        ?>
         <div id="footer-nav">
            <ul>
               <?php foreach ($navItems as $key => $value): ?>
                  <li><a class="<?= $page == $key ? 'highlight' : '' ?>" title="<?= $key ?>"
                     href="<?= $value ?>"><?= $key ?></a></li>
               <?php endforeach; ?>
            </ul>
        </div>

        <div id="footer-social">
           <ul>
              <li>
                 <a href="https://www.facebook.com/PowerHVbushings/" target="_blank" title="Facebook">
                    <i class="fab fa-facebook-square"></i>
                 </a>
              </li>
              <li>
                 <a href="" target="_blank" title="Twitter">
                    <i class="fab fa-twitter"></i>
                 </a>
              </li>
              <li>
                 <a href="" target="_blank" title="Instagram">
                    <i class="fab fa-instagram"></i>
                 </a>
              </li>
           </ul>
        </div>

         <div id="footer-info">
             <p>Email: info@phvbushing.com</p>
             <p>Phone: +1 709-769-2615</p>
             <p>350A Newfoundland Drive</p>
             <p>St. John’s NL, Canada A1A 4A2</p>
         </div>

         <div id="footer-copyright">
             <p>©2019 Power HV Inc. All Rights Reserved </p>
         </div>

      </div>
   </div>
</footer>
