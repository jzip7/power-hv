<?php 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 
 
 File: connect.php
 Description: 
    Connection to the DB

*********************************************************/

  // DB parameters
  define('DB_SRV','localhost');
  define('DB_NAME','powerhv');
  define('DB_USER','web_user');
  define('DB_PASS','mypass');

// database signature for MySQL
define('DB_DSN','mysql:host='.DB_SRV.';dbname='.DB_NAME);

// database handler
$dbh = new PDO(DB_DSN,DB_USER,DB_PASS);
// setting errmode
$dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

