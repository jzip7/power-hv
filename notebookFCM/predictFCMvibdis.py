from os import path
import pandas as pd
import numpy as np
import skfuzzy as fuzz
import json
from joblib import load

scaler = load('scalerVibDis')
cntr = load('centersVibDis')

## load JSON file to predict
with open('./notebookFCM/test.json') as f:
   data = json.load(f)
data = pd.DataFrame(data)
dataset = data[['Temp', 'Hum', 'Dis', 'Vib']]
scaled = scaler.transform(dataset)

dataX = np.array(scaled[:,3]) ## vibration
dataY = np.array(scaled[:,2]) ## partial discharge

alldata = np.vstack((dataX, dataY))

u, u0, d, jm, p, fpc = fuzz.cluster.cmeans_predict(alldata, cntr, 2, error=0.005, maxiter=1000)

## print predicted classes for each row of data from test.json
## 0, 1 = good
## 2, 3 = mechanical
## 4, 5 = electrical
## 6, 7 = both
for i in range(len(u[0])):
   for ind, val in enumerate(u[:,i]):
      print('class {} = {:.4%}'.format(ind, val))
      hs = max(u[:,i])
      if val == hs:
         hsi = ind
   print('done row {} -- class = {}'.format(i, hsi))