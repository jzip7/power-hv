## check RAM usage
import psutil
import os
pid = os.getpid()
ps = psutil.Process(pid)
print('Process id: {}'.format(pid))
mem_info = ps.memory_info()
mem1 = mem_info.rss*0.000001
print('Memory info: {:.5}MB'.format(mem1))
print('---------')
from joblib import load
import numpy as np
import skfuzzy as fuzz

scaler = load('notebookFCM/scaler')
cntr = load('notebookFCM/cntr')
data = load('notebookFCM/data')

scaled = scaler.transform(data.drop(columns=['Video 1', 'Bushing Condition']))
dataX = np.array(scaled[:,3]) ## vibration
dataY = np.array(scaled[:,2]) ## partial discharge
data = np.vstack((dataX, dataY))

u, u0, d, jm, p, fpc = fuzz.cluster.cmeans_predict(data, cntr, 2, error=0.005, maxiter=1000)

## check RAM usage
import psutil
import os
pid = os.getpid()
ps = psutil.Process(pid)
print('Process id: {}'.format(pid))
mem_info = ps.memory_info()
mem2 = mem_info.rss*0.000001
print('Memory info: {:.5}MB'.format(mem2))
print('---------')
print('Memory usage: {}MB'.format(mem2 - mem1))