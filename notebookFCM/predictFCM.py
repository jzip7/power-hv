from os import path
import pandas as pd
import numpy as np
import skfuzzy as fuzz
import json
from joblib import load

scaler = load('scaler')
cntr = load('centers')

## load JSON file to predict
with open('./notebookFCM/test.json') as f:
   data = json.load(f)
data = pd.DataFrame(data)
dataset = data[['Temp', 'Hum', 'Dis', 'Vib']]
print(dataset.shape)
scaled = scaler.transform(dataset)

dataX = np.array(scaled[:,0]) ## Temperature
dataY = np.array(scaled[:,1]) ## Humidity
dataZ = np.array(scaled[:,2]) ## Partial Discharge
dataQ = np.array(scaled[:,3]) ## Vibration

alldata = np.vstack((dataX, dataY, dataZ, dataQ))

u, u0, d, jm, p, fpc = fuzz.cluster.cmeans_predict(alldata, cntr, 2, error=0.005, maxiter=1000)

## print predicted classes for each row of data from test.json
for i in range(len(u[0])):
   for ind, val in enumerate(u[:,i]):
      print('class {} = {:.4%}'.format(ind, val))
      hs = max(u[:,i])
      if val == hs:
         hsi = ind
   print('done row {} -- class = {}'.format(i, hsi))

import matplotlib.pyplot as plt
import seaborn as sns

## Configure some general styling
sns.set_style("white")
plt.rcParams['font.size'] = 12
plt.rcParams['axes.labelsize'] = 20
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['xtick.labelsize'] = 15
plt.rcParams['ytick.labelsize'] = 15
plt.rcParams['legend.fontsize'] = 15
plt.rcParams['figure.titlesize'] = 20
plt.rcParams['figure.figsize'] = (8,7)

## DisVib
fig, ax = plt.subplots()
ax.set_title('Predicted Points (Dis/Vib)')

for j in range(4):
   ax.plot(alldata[3, u.argmax(axis=0) == j],
            alldata[2, u.argmax(axis=0) == j], 'o',
            label='class ' + str(j))
for pt in cntr:
   ax.plot(pt[3], pt[2], 'ks')
plt.show()

## TempHum
fig2, ax2 = plt.subplots()
ax2.set_title('Predicted Points (Temp/Hum)')

for j in range(4):
   ax2.plot(alldata[0, u.argmax(axis=0) == j],
            alldata[1, u.argmax(axis=0) == j], 'o',
            label='class ' + str(j))
for pt in cntr:
   ax2.plot(pt[0], pt[1], 'ks')
plt.show()