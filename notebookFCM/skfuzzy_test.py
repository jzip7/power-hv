import timeit
setup_code = '''
import skfuzzy as fuzz
import pandas as pd
import numpy as np

from sklearn import preprocessing

def rename_columns(dataset):
    # rename columns to a better format
    dataset.rename(columns={
                        'Temp.': 'temp',
                        'Rel. Hum.': 'humidity',
                        'Par. Dis.': 'partial_discharge',
                        'Vibr. Lev.': 'vibration',
                        'Audio 1': 'audio',
                        'Video 1': 'video',
                        'Bushing Condition': 'condition'},
                        inplace=True)
    return dataset

def output_invalid_condition(ind, val):
    """Output the index and value of an invalid condition to a .txt file
    
    ind -- the index the value is at in the dataset
    val -- the value
    """
    return None

def output_invalid_number(col, ind, val):
    """Output the index and value of an invalid value to a .txt file
    
    col -- the column the value exists in
    ind -- the index the value is at in the dataset
    val -- the value
    """
    return np.NaN

def convert_non_numeric(dataset):
    ## convert non numeric values to NaN
    dataset['temp'] = pd.to_numeric(dataset['temp'], errors='coerce')
    dataset['humidity'] = pd.to_numeric(dataset['humidity'], errors='coerce')
    dataset['partial_discharge'] = pd.to_numeric(dataset['partial_discharge'], errors='coerce')
    dataset['vibration'] = pd.to_numeric(dataset['vibration'], errors='coerce')
    return dataset

def convert_nan(dataset):
    ## convert values to NaN if not in the specified range
    # temperature between 19 and 31
    dataset['temp'] = [val if (19 <= val <= 31) else output_invalid_number('temp', ind, val) for ind, val in enumerate(dataset['temp'])]

    # relative humidity between 44 and 66
    dataset['humidity'] = [val if (44 <= val <= 66) else output_invalid_number('humidity', ind, val) for ind, val in enumerate(dataset['humidity'])]

    # partial discharge between 1 and 20
    dataset['partial_discharge'] = [val if (1 <= val <= 66) else output_invalid_number('partial_discharge', ind, val) for ind, val in enumerate(dataset['partial_discharge'])]

    # vibration level 0, 1, or 5
    dataset['vibration'] = [val if (val == 0 or val == 1 or val == 5) else output_invalid_number('vibration', ind, val) for ind, val in enumerate(dataset['vibration'])]
    # vibration level --> replace 5 with 1
    dataset['vibration'] = dataset['vibration'].replace(5, 1)

    # audio between 10 and 84
    dataset['audio'] = [val if (10 <= val <= 84) else output_invalid_number('audio', ind, val) for ind, val in enumerate(dataset['audio'])]
    return dataset

def convert_condition(dataset):
    conditions = [
        'good',
        'internal electrical insulation failure',
        'mechanical failure',
        'both electrical and mechanical failure',
    ]

    # convert to lower case for comparison to conditions list
    dataset['condition'] = dataset['condition'].str.lower()
    # filter the condtions to None if not in the conditions list
    dataset['condition'] = [val if val in conditions else output_invalid_condition(ind, val) for ind, val in enumerate(dataset['condition'])]
    return dataset

def clean_data(dataset):
    dataset = rename_columns(dataset)
    dataset = convert_non_numeric(dataset)
    dataset = convert_nan(dataset)
    dataset = convert_condition(dataset)
    return dataset

# import data 
dataset = pd.read_csv('./input/all_data.csv')

clean_data(dataset)

## labelled data
labelled = dataset.dropna(axis=0, how='any').drop(columns=['condition', 'video'])
xpts = labelled['vibration']
ypts = labelled['partial_discharge']
alldata = np.vstack((xpts, ypts))

df_unlabelled = pd.read_csv('./input/unlabeled_data.csv')
xpts_unlabelled = df_unlabelled['VibrLev']
ypts_unlabelled = df_unlabelled['ParDis']
unlabelled_data = np.vstack((xpts_unlabelled, ypts_unlabelled))

cntr, u_orig, _, _, _, _, _ = fuzz.cluster.cmeans(alldata, 4, 2, error=0.005, maxiter=1000)
'''
train_code = ''' 
fuzz.cluster.cmeans(alldata, 4, 2, error=0.005, maxiter=1000)
'''
predict_code = '''
fuzz.cluster.cmeans_predict(unlabelled_data, cntr, 2, error=0.005, maxiter=1000)
'''

print('Time to train model: {:5f} seconds'.format(timeit.timeit(setup = setup_code, 
                    stmt = train_code, 
                    number = 1)))
print('Time to predict: {:5f} seconds'.format(timeit.timeit(setup=setup_code,
                    stmt = predict_code,
                    number = 1)))

## find RAM usage
import psutil
import os
pid = os.getpid()
ps = psutil.Process(pid)
print('Process id: {}'.format(pid))
mem_info = ps.memory_info()
print('Memory info: {:.5} mb'.format(mem_info.rss*0.000001))