import os
import pandas as pd
import numpy as np
import skfuzzy as fuzz

from sklearn import preprocessing
from joblib import dump

def output_invalid_number(col, ind, val):
    """Output the index and value of an invalid value to a .txt file
        then return np.NaN to be replaced in dataset
    
    col -- the column the value exists in
    ind -- the index the value is at in the dataset
    val -- the value
    """
    f = open('./output/invalid_' + col + '.txt', 'a+')
    f.write(str(ind) + ',' + str(val) + '\n')
    return np.NaN

def output_invalid_condition(ind, val):
    """Output the index and value of an invalid condition to a .txt file
        then return None to be replaced in dataset
    
    ind -- the index the value is at in the dataset
    val -- the value
    """
    f = open('./notebookFCM/output/invalid_condition.txt', 'a+')
    f.write(str(ind) + ',' + str(val) + '\n')
    return None

# import data 
dataset = pd.read_csv('./input/all_data.csv')

# rename columns to a better format
dataset.rename(columns={
                    'Temp.': 'temp',
                    'Rel. Hum.': 'humidity',
                    'Par. Dis.': 'partial_discharge',
                    'Vibr. Lev.': 'vibration',
                    'Audio 1': 'audio',
                    'Video 1': 'video',
                    'Bushing Condition': 'condition'},
                    inplace=True)

# remove any output files previously created when checking for invalid data
for column in dataset.columns.values:
    filepath = './output/invalid_' + column + '.txt'
    if os.path.exists(filepath):
        os.remove(filepath)

## convert non numeric values to NaN
dataset['temp'] = pd.to_numeric(dataset['temp'], errors='coerce')
dataset['humidity'] = pd.to_numeric(dataset['humidity'], errors='coerce')
dataset['partial_discharge'] = pd.to_numeric(dataset['partial_discharge'], errors='coerce')
dataset['vibration'] = pd.to_numeric(dataset['vibration'], errors='coerce')

## convert values to NaN if not in the specified range
# temperature between 19 and 31
dataset['temp'] = [val if (19 <= val <= 31) else output_invalid_number('temp', ind, val) for ind, val in enumerate(dataset['temp'])]

# relative humidity between 44 and 66
dataset['humidity'] = [val if (44 <= val <= 66) else output_invalid_number('humidity', ind, val) for ind, val in enumerate(dataset['humidity'])]

# partial discharge between 1 and 20
dataset['partial_discharge'] = [val if (1 <= val <= 66) else output_invalid_number('partial_discharge', ind, val) for ind, val in enumerate(dataset['partial_discharge'])]

# vibration level 0, 1, or 5
dataset['vibration'] = [val if (val == 0 or val == 1 or val == 5) else output_invalid_number('vibration', ind, val) for ind, val in enumerate(dataset['vibration'])]
# vibration level --> replace 5 with 1
dataset['vibration'] = dataset['vibration'].replace(5, 1)

# audio between 10 and 84
dataset['audio'] = [val if (10 <= val <= 84) else output_invalid_number('audio', ind, val) for ind, val in enumerate(dataset['audio'])]

conditions = [
    'good',
    'internal electrical insulation failure',
    'mechanical failure',
    'both electrical and mechanical failure',
]

# convert to lower case for comparison to conditions list
dataset['condition'] = dataset['condition'].str.lower()
# filter the condtions to None if not in the conditions list
dataset['condition'] = [val if val in conditions else output_invalid_condition(ind, val) for ind, val in enumerate(dataset['condition'])]

## final filtered dataset
dataset = dataset.drop(columns=['condition', 'video', 'audio'])

## scale dataset
scaler = preprocessing.MinMaxScaler()
dataset = scaler.fit_transform(dataset)

## extract the columns to be used in the algorithm
xpts = dataset[:,3]
ypts = dataset[:,2]

alldata = np.vstack((xpts, ypts))

## define number of clusters to use
clusters = 4

## determine centers of the clusters
cntr, u_orig, u0, d, jm, p, fpc = fuzz.cluster.cmeans(
    alldata, clusters, 2, error=0.005, maxiter=1000)

## output required objects to use for prediction
dump(scaler, 'scalerVibDis')
dump(cntr, 'centersVibDis')